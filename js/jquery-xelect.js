﻿// todo - get rid of jQuery in core selector
(function ($) {
    /**
     * 
     * @param {} f 
     * @returns {} 
     */
    function callEach(f) {
        for (var i = 0; i < this.length; i++) {

            var el = this[i];
            if (el.xelect) {
                el.xelect[f].apply(el.xelect, Array.prototype.slice.call(arguments, 1));
            }
        }
    }

    var methods = {
        init: function (options) {

            for (var i = 0; i < this.length; i++) {

                var el = this[i];
                var $el = $(el);

                // already bound
                if ($el.hasClass("xelect-bound")) {
                    continue;
                }

                // mark as bound
                $el.addClass("xelect-bound");

                var settings = {};
                $.extend(settings, options);

                settings.maxHeight = settings.maxHeight || parseInt($el.data("max-height")) || 20;

                el.xelect = new brightspark.Xelect(el, settings);
            }

            return this;
        },

        setRenderItem: function(renderItemFunc) {
            callEach.call(this, "setRenderItem", renderItemFunc);
        },

        open: function () {
            callEach.call(this, "open");
        },

        close: function () {
            callEach.call(this, "close");
        },
        
        clear: function() {
            callEach.call(this, "clear");
        },
        
        change: function(cb) {
            callEach.call(this, "change", cb);
        },
        
        getOptions: function(cb) {
            
            if (this.length === 0) {
                return undefined;
            }

            var el = this[0];
            if (el.xelect) {
                return el.xelect.getOptions();
            }

            return undefined;
        }
    };

    $.fn.xelect = function (method) {
        // Method calling logic
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.xelect');
            return this;
        }
    };

    $.xelect = function (method) {
        // Method calling logic
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.xelect');
            return this;
        }
    };
})(jQuery);
/*
$(function () {
    $(".selector").xelect();
});
*/
$(document).click(function () {
    var $notClickedDdContainers = $(".xelect:not(.clicked)");
    $notClickedDdContainers.xelect("close");
    $(".xelect").removeClass("clicked");
});

$.expr[":"].icontains = $.expr.createPseudo(function (arg) {
    return function (elem) {
        return $(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
    };
});