module.exports = function (grunt) {

    require('load-grunt-tasks')(grunt);
    
    grunt.initConfig({
        
        sass: {
            options: {
                sourceMap: true
            },
            dist: {
                files: {
                    'src/css/sparkling-xelect.css': 'src/scss/main.scss'
                }
            }
        },
          
        clean: {
            main: ["deploy"],
            empty: {
                src: ['deploy/**'],
                filter: function (fp) {
                    return grunt.file.isDir(fp) && require('fs').readdirSync(fp).length === 0;
                }
            },
        },
        includes: {
            js: {
                options: {
                    includeRegexp: /(\s*)__grunt_include\s*\("(\S+)"\);*\s*$/
                },
                files: [{
                    cwd: 'src',
                    src: ['**/*.js', "!include/**/*"],
                    dest: 'deploy/',
                }],
            },
        },
        jsbeautifier: {
            src : ["deploy/**/*.js"]
        },
        copy: {
            main: {
                expand: true,
                cwd: 'src/',
                src: ['**/*', '!**/*.js', '!scss/**'],
                dest: 'deploy/',
                filter: function (filepath) {
                    return !grunt.file.isDir(filepath) || require('fs').readdirSync(filepath).length > 0;
                },
            },
            packagejson: {
                src: 'package.json',
                dest: 'deploy/package.json',
            }
        },
        uglify: {
            options: {
                mangle: true,
                compress: false,
                sourceMap: true
            },
            js: {
                files: {
                    'deploy/sparkling-xelect.min.js': ['deploy/*.js']
                }
            }
        },
        cssmin: {
            options: {
                shorthandCompacting: false,
                roundingPrecision: -1,
                sourceMap: true
            },
            target: {
                files: [{
                    expand: true,
                    cwd: 'deploy/css',
                    src: ['*.css', '!*.min.css'],
                    dest: "deploy/css",
                    ext: '.min.css'
                }]
            }
        },
        postcss: {
            options: {
                map: false,
                processors: [
                    require("postcss-cssnext")({ browsers: ['last 5 versions'] }),
                ]
            },
            dist: {
                src: ['deploy/css/*.css', 'deploy/css/!*.min.css'],
            }
        },

        run: {
            options: {
                cwd: "deploy"
            },
            npm: {
                exec: "npm pack && npm publish"
            }
        }
    });

    grunt.registerTask("npmrc", function () {
        grunt.file.write(process.env.USERPROFILE + "/.npmrc", "@brightspark:registry=https://www.myget.org/F/brightspark/npm/\r\n\
//www.myget.org/F/brightspark/npm/:_authToken=b101a971-03c8-4bef-9e04-4e39625faf02\r\n\
//www.myget.org/F/brightspark/npm/:always-auth=true")
    });

    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-includes');
    grunt.loadNpmTasks("grunt-jsbeautifier");
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-postcss');
    grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-run');
	
	//grunt.registerTask('scss', ['sass']);
	grunt.registerTask('deploy', ['default', 'npmrc', 'run:npm']);
    grunt.registerTask('default', ['clean:main', 'includes', 'jsbeautifier', 'copy', 'clean:empty', 'uglify', "sass", 'postcss', 'cssmin' ]);
};