describe("Combobox", function () {
	var Xelect = sparkling.Xelect;
	var elContainer;
	var divElement;
	var comboboxInstance;
	var xelectDom;
	beforeEach(function () {
		elContainer = document.createElement("div");
		divElement = document.createElement("div");
		elContainer.appendChild(divElement);

		comboboxInstance = new Xelect(divElement, {
			type: "combobox"
		});
		xelectDom = elContainer.querySelector(".xelect");
	});

	afterEach(function () {
		elContainer = undefined;
		divElement = undefined;
		comboboxInstance = undefined;
		xelectDom = undefined;
	});

	it("is instanceof Xelect", function () {
		expect(comboboxInstance instanceof Xelect).toBe(true);
	});

	it("has type of combobox", function () {
		expect(comboboxInstance.type).toBe("combobox");
	});

	it("has CSS classname 'xelect-combobox'", function () {
		expect(xelectDom.classList.contains("xelect-combobox")).toBe(true);
	});
});