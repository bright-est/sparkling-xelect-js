if (typeof sparkling === "undefined") {
	sparkling = {};
}

(function (ns, w) {

	var __version = "1.1.61";

	var _h = __grunt_include("include/sparkling-xelect-html-helper.js");
	var _regexHelper = __grunt_include("include/sparkling-xelect-regex-helper.js");
	var _urlHelper = __grunt_include("include/sparkling-xelect-url-helper.js");

	var XelectItemView = __grunt_include("include/sparkling-xelect-item-view.js");
	var XelectItem = __grunt_include("include/sparkling-xelect-item.js");

	var Observable = sparkling.Observable;
	var Ajax = sparkling.ajax;

	var __repo = {};

	/**
	 * Constants
	 */
	var _const = {
		KEY: {
			BACKSPACE: 8,
			TAB: 9,
			ENTER: 13,
			SHIFT: 16,
			ESCAPE: 27,
			SPACE: 32,
			PAGE_UP: 33,
			PAGE_DOWN: 34,
			END: 35,
			HOME: 36,
			LEFT: 37,
			UP: 38,
			RIGHT: 39,
			DOWN: 40,
			DELETE: 46,
			NUMPAD_ENTER: 108
		},

		XELECT_TYPE: {
			COMBOBOX: 1,
			AUTOCOMPLETE: 2,
			SELECT: 3
		},

		defaultIcons: {
			x: '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 6 6" width="6px" height="6px" xml:space="preserve"><polygon class="x" points="6,0.7 5.3,0 3,2.3 0.7,0 0,0.7 2.3,3 0,5.3 0.7,6 3,3.7 5.3,6 6,5.3 3.7,3 "/></svg>',
			chevron: '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="10px" height="6px" x="0px" y="0px" viewBox="0 0 10 6" xml:space="preserve"><polygon points="5.1,6 0,1.3 1.4,0 5.1,3.3 8.6,0 10,1.3"/></svg>',
			ac: '<svg version="1.1" xml:space="preserve" width="14px" height="10px" viewBox="0 0 128 128" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg"><path d="M118.975,105.396L98.646,85.068c5.603-8.161,8.9-18.067,8.897-28.689  c0.005-13.972-5.692-26.713-14.854-35.86c-9.146-9.161-21.887-14.858-35.86-14.854c-13.972-0.004-26.713,5.692-35.858,14.854  C11.81,29.665,6.114,42.406,6.118,56.379C6.114,70.353,11.81,83.093,20.972,92.238c9.146,9.162,21.887,14.859,35.858,14.853  c10.157,0.004,19.654-3.015,27.601-8.175l20.512,20.511c3.875,3.878,10.157,3.878,14.031,0  C122.852,115.553,122.852,109.271,118.975,105.396z M35.004,78.206c-5.601-5.616-9.036-13.272-9.041-21.827  c0.005-8.555,3.44-16.21,9.042-21.827c5.617-5.602,13.271-9.037,21.825-9.042c8.556,0.005,16.21,3.44,21.828,9.041  c5.601,5.618,9.036,13.272,9.04,21.828c-0.004,8.555-3.439,16.211-9.04,21.827c-5.617,5.602-13.271,9.036-21.828,9.04  C48.276,87.242,40.622,83.808,35.004,78.206z" fill="#232323"/></svg>'
		}
	};

	_const.XELECT_TYPE_MAP = {
		'1': _const.XELECT_TYPE.COMBOBOX,
		'combobox': _const.XELECT_TYPE.COMBOBOX,
		'2': _const.XELECT_TYPE.AUTOCOMPLETE,
		'autocomplete': _const.XELECT_TYPE.AUTOCOMPLETE,
		'3': _const.XELECT_TYPE.SELECT,
		'select': _const.XELECT_TYPE.SELECT
	};

	var _defaults = {
		id: undefined,
		name: undefined,
		multiselect: false,
		forceValue: true,
		allowEmpty: true,
		type: _const.XELECT_TYPE.SELECT,
		src: undefined,
		maxHeight: undefined,
		validation: undefined,
		placeholder:undefined,
		labels: {
			hint: undefined,
			searching: undefined,
			noResults: undefined
		},
		disabled: false,
		// event handlers
		change: undefined,
		beforeSelect: undefined,
		select: undefined,
		beforeClear: undefined,
		clear: undefined,
		defaultKey: undefined,
		delimiters: [],
		ac: {
			url: undefined,
			method: "POST",
			queryKey: "q",
			crossDomain: false,
			dataType: "json",
			contentType: "application/x-www-form-urlencoded",
			transformResult: undefined,
			dataFunc: undefined,
			src: undefined,
			match: undefined,
			sort: undefined,
			limit: undefined,
		},
		readOnly: false,
		renderSelectedValueAsHTML: false
	};

	var _static = new Observable();

	_static.change = function (handler) {
		_static.subscribe("change", handler);
	};

	/**
	 * THE object
	 * @param {string|HTMLElement} el
	 * @param {object} settings
	 * @returns {}
	 */
	var Xelect = function Xelect() {

		if (!(this instanceof Xelect)) {
			// called as function
			var requestedId = arguments[0];
			return __repo[requestedId];
		}

		var _this = this;
		Observable.mixin(this);

		// alias
		this.on = this.subscribe;

		var _el_xelect,
			_el_xelectInputContainer,
			_el_textInput,
			_el_valueInput,
			_el_xelectTitle,
			_el_optionsContainer,
			_el_optionsWrapper,
			_el_optionsList,
			_el_selectedValuesList,
			_el_clearValue,
			_el_chevron,
			_el_HelpText;

		var _options = [];
		var _optionsDict = {};

		var _disabled;
		var _isOpened;
		var _selectedValues = [];
		var _selectedValue;

		var _activeOption;
		var _activeSelectedValue;

		var _settings;

		var _isMultiselect;
		var _isAutocomplete;
		var _isCombobox;
		var _isDropdown;

		var _execAutocompleteRequestTimeout;
		var _acXhr;
		var _prevText = "";

		var _lastScrollTarget;

		Object.defineProperties(this, {
			isOpened: {
				get: function () { return _isOpened; }
			},
			type: {
				get: function () { return _settings.type; }
			},
			isMultiselect: {
				get: function () { return _isMultiselect; }
			},
			el_input: {
				get: function () { return _el_textInput; }
			}
		});

		function __construct(el, settings) {
			// resolve element
			if (typeof el === "string") {
				el = document.getElementById(el);
			}
			if (!el) {
				el = document.createElement("div");
			}

			if (el.id) {
				__repo[el.id] = _this;
			}

			_defaults.renderItem = defaultRenderItem;
			_defaults.renderSelectedValue = defaultRenderSelectedValue;
			_defaults.ac.renderItem = acDefaultRenderItem;
			_settings = buildSettings(settings, getInlineSettings(el), _defaults);

			_isMultiselect = _settings.multiselect;
			_isAutocomplete = _const.XELECT_TYPE_MAP[_settings.type] === _const.XELECT_TYPE.AUTOCOMPLETE;
			_isCombobox = _const.XELECT_TYPE_MAP[_settings.type] === _const.XELECT_TYPE.COMBOBOX;
			_isDropdown = _const.XELECT_TYPE_MAP[_settings.type] === _const.XELECT_TYPE.SELECT;

			_this.on("change", function (selection) {
				_static.publish("change", _this, selection);
			});

			_this.subscribe("change", _settings.change);
			_this.subscribe("beforeSelect", _settings.beforeSelect);
			_this.subscribe("select", _settings.select);
			_this.subscribe("beforeClear", _settings.beforeClear);
			_this.subscribe("clear", _settings.clear);

			if (_h.hasClass(el, "xelect")) {
				// existing markup

				_el_xelect = el;

				_isMultiselect = _h.hasClass(_el_xelect, "xelect-multiselect");
				_isDropdown = _h.hasClass(_el_xelect, "xelect-select");
				_isAutocomplete = _h.hasClass(_el_xelect, "xelect-autocomplete");

				_el_xelectInputContainer = _(".xelect-input-container", _el_xelect);

				_el_optionsContainer = _(".xelect-options", _el_xelect);
				_el_xelectTitle = _(".xelect-title", _el_xelect);
				_el_clearValue = _(".clear-value", _el_xelectTitle);
				_el_chevron = _(".chevron", _el_xelectTitle);
				_el_textInput = _("input[type=text]", _el_xelect);
				_el_valueInput = _("input.xelect-value", _el_xelect);

				_el_optionsWrapper = _el_optionsContainer.children[0];
				_el_HelpText = _(".help-text", _el_optionsWrapper);
				_el_optionsList = _("ul", _el_optionsWrapper);

				var elLis = __("li", _el_optionsList);
				for (var i = 0, len = elLis.length; i < len; i++) {
					var elLi = elLis[i];
					var value = elLi.getAttribute("key");

					var elText = _(".original-text", elLi);
					var text = elText ? elText.innerHTML : "";

					var elLabel = _(".option-label", elLi);
					var label = elLabel ? elLabel.innerHTML : "";

					var selected = elLi.classList.contains("selected");
					var disabled = elLi.classList.contains("disabled");
					var isGroup = elLi.classList.contains("group");

					var data = { 
						value: value,
						text: text,
						label: label,
						selected: selected,
						disabled: disabled,
						isGroup: isGroup
					 };

					for(var attrIndex = 0; attrIndex < elLi.attributes.length; attrIndex++) {
						var attr = elLi.attributes[attrIndex];
						if (attr.name.startsWith("data-")) {

                            var dataKey = attr.name.substring(5).replace(/(\-[a-z])/g, function(m) {
								return m.substring(1).toUpperCase();
							});

							data[dataKey] = attr.value;
						}
					}

					var item = new XelectItem(elLi, data);
					addOption(item);

					if (selected && !_isMultiselect) {
						_selectedValue = item;
					}
				}

				if (_isAutocomplete && !_isMultiselect) {
					if (_el_valueInput.value) {
						_selectedValue = new XelectItem(null, {
							value: _el_valueInput.value,
							text: _el_textInput.value
						});
					}
				}

				_el_selectedValuesList = _(".xelect-selected-values", _el_xelect);
				if (_el_selectedValuesList) {

					var selectedValuesLiElements = __("li", _el_selectedValuesList);
					for (var i = 0; i < selectedValuesLiElements.length; i++) {

						(function(li) {

							var refKey = _(".selected-token-key", li);
							var refVal = _(".selected-token-value", li);
							var key = refKey.value;

							var xelectItem = _optionsDict[key];
							if (!xelectItem) {
								// xelect item cannot be found form options when we have autocomplete
								xelectItem = new XelectItem(null,
									{
										text: refVal.value,
										value: key,
									});
								_selectedValues.push(xelectItem);
							}

							xelectItem.elSelectedValue = li;

							li.xelectItem = xelectItem;
							li.ref_key = refKey;
							li.ref_val = refVal;

							var removeElement = _(".clear-value", li)
										
							_h.on(removeElement, "click", function (e) {
								e.stopPropagation();

								if (_disabled || _h.hasClass(li, "disabled")) {
									return;
								}

								removeValue(li);
							});
						})(selectedValuesLiElements[i]);
					}
				}

				_disabled = _h.hasClass(_el_xelect, "disabled");

				var hint = _h.getSibling(_el_xelect, '.ac-hint-text');
				var searching = _h.getSibling(_el_xelect, '.ac-searching-text');
				var noResults = _h.getSibling(_el_xelect, '.ac-no-results-text');

				_settings.labels = {
					hint: hint ? hint.innerHTML : null,
					searching: searching ? searching.innerHTML : null,
					noResults: noResults ? noResults.innerHTML : null
				};

				document.body.appendChild(_el_optionsContainer);

			} else {

				buildXelect(el);

				var src = _settings.src;
				var createdItems = addOptions(src);
				renderOptions(createdItems);

				el.parentNode.replaceChild(_el_xelect, el);
			}

			_el_xelect.xelect = _this;
			
			(function bindOptionEvents() {
				for (var i = 0; i < _options.length; i++) {
					bindSingleOptionEvents(_options[i]);
				}
			})();

			_el_textInput.addEventListener("keydown", keyDown);
			_el_textInput.addEventListener("keyup", keyUp);

			_el_textInput.addEventListener("blur", function () {
				_this.close();
			});

			_el_textInput.addEventListener("focus", function () {
				if (!_isOpened) {
					_this.open();
				}
			});

			_el_textInput.addEventListener("click", function () {
				if (document.activeElement === this && !_isOpened) {
					_this.open();
				}
			});

			window.addEventListener('resize', function() {
				_this.close();
			});

			window.addEventListener('blur', function() {
				// Fixes a bug when you select input, then click somewhere out of the window, and then select another input.
				_el_textInput.blur();
			});

			// Combobox does't have autocomplete
			if (!_isCombobox && !_isAutocomplete) {
				
				_el_chevron.addEventListener("click", function () {
					_el_textInput.focus();
				});
			}

			if (!_isMultiselect) {

				if (_el_clearValue) {

					// clear value is for ss-autocomplete and ss-combobox
					_el_clearValue.addEventListener("mousedown", function () {
						if (_disabled) {
							return false;
						}
						_this.clear();
						return false;
					});
				}
			}

			if (_settings.disabled) {
				_this.disable();
			}
		}

		// API --

		this.updateSettings = function (settings) {
			_settings = buildSettings(settings, null, _settings);
		}

		/**
		 *
		 * @param {function} f
		 */
		this.setRenderItem = function (f) {
			_settings.renderItem = f;
		};

		/**
		 *
		 * @returns {}
		 */
		this.open = function () {

			if (_disabled) {
				return;
			}

			console.log("add scroll event listener");
			document.addEventListener("wheel", onScroll, true);

			// element is in iframe
			if (window !== window.parent) {

				var winTop = 0;
				var winLeft = 0;

				var rootWindow = _el_xelect.ownerDocument.defaultView;
				while (rootWindow !== rootWindow.parent) {
					var o = window.frameElement.getBoundingClientRect();
					winTop += o.top;
					winLeft += o.left;
					rootWindow = rootWindow.parent;
				}

				// Take parent window scroll into account for absolute position
				scroll = _h.getWindowScroll(rootWindow);
				winTop += scroll.top;
				winLeft += scroll.left;

				var offset = _el_xelect.getBoundingClientRect();
				var width = _h.width(_el_xelect);
			
				_h.css(_el_optionsContainer, {
					top: offset.height + offset.top + winTop + "px",
					left: offset.left + winLeft + "px",
					width: width + "px",
					"z-index": 10000000
				});
				
				rootWindow.document.body.appendChild(_el_optionsContainer)

				var onCloseHandler = function () {
					rootWindow.document.body.removeChild(_el_optionsContainer);
					_this.unsubscribe("close", onCloseHandler);
				}
				_this.onClose(onCloseHandler);
			}
			else {
                applyStyles(_el_xelectInputContainer, _el_optionsContainer);
            }

			_isOpened = true;
			_h.addClass(_el_xelect, "opened");
			_h.addClass(_el_optionsContainer, "opened");

			_h.addClass(_el_xelect, "clicked");

			var availableItems = (_isMultiselect) ?
				_h.getChildren(_el_optionsList, ":not(.selected):not(.no-match)") :
				_h.getChildren(_el_optionsList, ":not(.no-match)");

			if (_isAutocomplete) {
				if (availableItems.length == 0) {
					showHelpText(_settings.labels.hint);
				}
			} else {
				calcMaxHeight(availableItems);
			}

			requestAnimationFrame(function () {
				_el_textInput.focus();
			})
		};

		/**
		 *
		 * @returns {}
		 */
		this.close = function () {

			console.log("close");

			if (_h.hasClass(_el_xelect, "force-open")) {
				return;
			}

			if (!_isOpened) {
				return;
			}

			console.log("remove scroll event listener");
			_lastScrollTarget = null;
			document.removeEventListener("wheel", onScroll, true);

			_isOpened = false;
			_h.removeClass(_el_xelect, "opened");
			_h.removeClass(_el_optionsContainer, "opened");

			_clearAcTimeout();

			if (_isAutocomplete) {
				// autocomplete displays hint when focused / searching / has no results
				// when element is blurred, hide hint container aswell
				hideHelpText();
			} else if (_isDropdown) {
				// to support highlighting, combobox options have two sorts of texts: original and highlighted
				// filtering is done by original text
				// when list is closed, cleanup all highlighted texts and show original text

				for (var i = 0; i < _options.length; i++) {
					_options[i].clearHighlight();
				}
			} else if (_isCombobox) {
				if (_el_textInput.value) {
					createComboboxItem();
				}
			}

			if (!_isMultiselect) {
				// restore selected value title if it is modified
				if (_el_textInput.value === "" && _settings.allowEmpty !== false) {
					_this.clear();
				} else if (_settings.forceValue === true) {
					_el_textInput.value = _selectedValue 
						? (_selectedValue.label || _selectedValue.text)
						: "";
				}
			} else {
				// cleanup
				_el_textInput.value = "";
			}

			var el_li = _h.getChildren(_el_optionsList, "li");
			for (var i = 0, l = el_li.length; i < l; i++) {
				_h.removeClass(el_li[i], 'no-match');
			}

			setActiveOption(undefined);

			_this.publish("close");
		};

		/**
		 * Clears all selected values
		 * @returns {}
		 */
		this.clear = function () {

			var beforeClearResult = _this.publish("beforeClear");

			if (beforeClearResult === false) {
				return false;
			}

			if (_isMultiselect) {

				_el_textInput.value = "";

				for (var i = 0; i < _selectedValues.length; i++) {

					var xelectItem = _selectedValues[i];
					xelectItem.selected = false;
					// todo - handle via subscriber

					var elSelectedValue = xelectItem.elSelectedValue;

					if (elSelectedValue.ref_key) {
						_h.remove(elSelectedValue.ref_key);
					}
					if (elSelectedValue.ref_val) {
						_h.remove(elSelectedValue.ref_val);
					}

					_h.remove(elSelectedValue);
				}
				_selectedValues = [];
			} else {

				if (_settings.allowEmpty === false) {
					return false;
				}

				if (_selectedValue) {
					_selectedValue.selected = false;
					// todo - rest of the functionality should be in subscriber
				}

				_el_textInput.value = "";
				_el_valueInput.value = "";

				_selectedValue = undefined;

				_h.removeClass(_el_xelectTitle, "has-value");

				if (_isAutocomplete) {
					// clear options
					_el_optionsList.innerHTML = "";
				}
			}

			_this.publish("clear");
			_this.close();
			_this.change();

			return true;
		};

		/**
		 * Set element as disabled i.e. disable functionality
		 * @returns {}
		 */
		this.disable = function () {
			_disabled = true;
			_el_textInput.setAttribute("disabled", "disabled");
			_h.addClass(_el_xelect, "disabled");
		};

		/**
		 * Set element as enabled i.e. enable functionality
		 * @returns {}
		 */
		this.enable = function (enable) {

			if (typeof enable === "undefined") {
				enable = true
			}

			if (!enable) {
				_this.disable();
			}
			else {
				_disabled = false;
				_el_textInput.removeAttribute("disabled");
				_h.removeClass(_el_xelect, "disabled");
			}
		};

		/**
		 * Select item by value
		 * @param {string} val
		 * @returns {bool} whether value was selected on not
		 */
		this.selectValue = function (val) {
			var item = _optionsDict[val];
			if (!item) {
				return false;
			}
			return _this.selectItem(item);
		};

		/**
		 * Select item
		 * @param {XelextItem} item
		 * @returns {bool} whether value was selected on not
		 */
		this.selectItem = function (item) {
			if (item.selected) {
				return false;
			}

			selectItem(item);
			return true;
		};

		this.getOptions = function () {
			return _options;
		};

		this.addOption = function (item) {
			addOption(item);
			renderOptions([item]);
		};

		/**
		 * Register new options for xelect
		 */
		this.addOptions = function (options) {
			var createdItems = addOptions(options);
			renderOptions(createdItems);
			for (var i = 0; i < createdItems.length; i++) {
				bindSingleOptionEvents(createdItems[i]);
			}
		};

		this.removeOption = function (item) {
			
			_options = grep(_options, function (x) {
				return x.value !== item.value;
			});

			delete _optionsDict[item.value];

			if (item.selected) {
				removeSelectedValue(item);
			}

			_h.remove(item.elOption);
		}

		this.clearOptions = function() {
			
			// clear selected values
			_this.clear();
			
			_options = [];
			_optionsDict = {};

			_el_optionsList.innerHTML = "";
		};

		/** Obsolete */
		this.getSelectedValue = function () {
			return _selectedValue;
		};

		/** Obsolete */
		this.getSelectedValues = function () {
			return _selectedValues;
		};
		
		/**
		 * Get single selected item.
		 * Returns null if multiselect.
		 */
		this.getSelectedItem = function () {
			return _selectedValue;
		};

		/**
		 * Get selected items array.
		 * Returns null if non-multiselect
		 */
		this.getSelectedItems = function () {
			return _selectedValues;
		};

		/**
		 *
		 * @param {} value
		 * @returns {}
		 */
		this.removeSelectedValue = function (value) {

			var li;
			if (value instanceof XelectItem) {
				li = value.elOption;
			} else {
				li = _("li[key='" + value + "']", _el_optionsList);
			}

			removeValue(li);
		};

		/**
		 *
		 * @param {} value
		 * @returns {}
		 */
		this.removeSelectedValues = function (arr) {
			for (var i = 0; i < arr.length; i++) {
				_this.removeSelectedValue(arr[i]);
			}
		};

		/**
		 * @param test
		 * @param match
		 * @param noMatch
		 */
		this.filter = filter;

		// EVENTS

		/**
		 *
		 * @param {} cb
		 * @returns {}
		 */
		this.change = function() {

			if (typeof arguments[0] === "function") {
				var handler = arguments[0];
				_this.on("change", handler);
				return this;
			}

			var selection = _this.isMultiselect ? _this.getSelectedValues() : _this.getSelectedValue();
			return _this.publish("change", selection);
		}

		this.beforeSelect = function (handler) {
			_this.on("beforeSelect", handler);
			return this;
		};

		this.onSelect = function(handler) {
			_this.on("select", handler);
			return this;
		};
		
		this.beforeClear = function(handler) {
			_this.on("beforeClear", handler);
			return this;
		};

		this.onClear = function(handler) {
			_this.on("clear", handler);
			return this;
		};
		
		this.beforeClose = function (handler) {
			_this.on("beforeClose", handler);
			return this;
		};

		this.onClose = function(handler) {
			_this.on("close", handler);
			return this;
		};

		// --EVENTS

		// --API

		function buildXelect(el) {

			var elClassName = el.className;
			var id = el.id;
			var tabIndex = el.tabIndex < 0 ? undefined : el.tabIndex;

			_el_xelect = _h.parseHtml("<div class=\"xelect " + elClassName + "\"></div>");
			_el_optionsContainer = _h.parseHtml("<div class=\"xelect-options\"></div>");

			if (_isMultiselect) {
				// ensure "xelect-multiselect" class name if multiselect
				_h.addClass(_el_xelect, "xelect-multiselect");
				_h.addClass(_el_optionsContainer, "xelect-options-multiselect");
			}

			if (_isDropdown) {
				// ensure "xelect-select" class name for select field
				_h.addClass(_el_xelect, "xelect-select");
			} else if (_isAutocomplete) {
				// ensure "xelect-autocomplete" class name for autocomplete field
				_h.addClass(_el_xelect, "xelect-autocomplete");
				_h.addClass(_el_optionsContainer, "xelect-options-autocomplete");
			} else if (_isCombobox) {
				// ensure "xelect-combobox" class name for combobox field
				_h.addClass(_el_xelect, "xelect-combobox");
				_h.addClass(_el_optionsContainer, "xelect-options-combobox");
			}

			if (_settings.allowEmpty === false) {
				_h.addClass(_el_xelect, "xelect-no-empty");
			}
			
			_el_textInput = _h.parseHtml("<input class=\"xelect-input\" type=\"text\" autocomplete=\"off\" tabindex=\"" + tabIndex + "\" id=\"" + id + "\">");
			// _el_textLabel = _h.parseHtml("<div class=\"xelect-label\" id=\"" + id + "\"></div>");
			_el_valueInput = _h.parseHtml("<input type=\"hidden\" name=\"" + id + "\">");
			_el_xelectTitle = _h.parseHtml("<div class=\"xelect-title\"><span></span>&nbsp;</div>");

			if (_settings.placeholder && _settings.placeholder !== "") {
				_el_textInput.setAttribute("placeholder",_settings.placeholder);
			}

			if(_settings.readOnly){
				_el_textInput.setAttribute("readOnly", true);
			}

			if (_isAutocomplete) {
				_el_clearValue = _h.parseHtml("<div class=\"clear-value\">" + _const.defaultIcons.x + "</div>");
				var _el_ac = _h.parseHtml("<div class=\"chevron\">" + _const.defaultIcons.ac + "</div>");

				_el_xelectTitle.appendChild(_el_clearValue);
				_el_xelectTitle.appendChild(_el_ac);
			}
			else if (!_isCombobox) {
				_el_clearValue = _h.parseHtml("<div class=\"clear-value\">" + _const.defaultIcons.x + "</div>");
				_el_chevron = _h.parseHtml("<div class=\"chevron\">" + _const.defaultIcons.chevron + "</div>");

				_el_xelectTitle.appendChild(_el_clearValue);
				_el_xelectTitle.appendChild(_el_chevron);
			}
			
			_el_optionsWrapper = _h.parseHtml("<div class=\"ul-wrapper\"></div>");
			_el_optionsList = _h.parseHtml("<ul field=\"" + id + "\"></ul>");
			_el_selectedValuesList = _h.parseHtml("<ul class=\"xelect-selected-values\"></ul>");

			_el_HelpText = _h.parseHtml("<div class=\"help-text\"></div>");

			_el_xelect.appendChild(_el_selectedValuesList);

			_el_xelectInputContainer = document.createElement("div");
			_el_xelectInputContainer.appendChild(_el_textInput);
			_el_xelectInputContainer.appendChild(_el_valueInput);
			_el_xelectInputContainer.appendChild(_el_xelectTitle);

			document.body.appendChild(_el_optionsContainer);

			_h.addClass(_el_xelectInputContainer, "xelect-input-container");
			_el_xelect.appendChild(_el_xelectInputContainer);

			_el_optionsContainer.appendChild(_el_optionsWrapper);

			_el_optionsWrapper.appendChild(_el_optionsList);
			_el_optionsWrapper.appendChild(_el_HelpText);
		}

		function showHelpText(text) {
			_el_HelpText.innerHTML = text;
			_h.addClass(_el_HelpText, "help-text--visible");
		}

		function hideHelpText() {
			_el_HelpText.innerHTML = "";
			_h.removeClass(_el_HelpText, "help-text--visible");
		}

		function defaultRenderSelectedValue(item) {

			// create form values
			var name;
			if (_isCombobox) {
				name = _settings.name + "[" + _options.length + "]";
			} else {
				name = _settings.name + "[" + _selectedValues.length + "]";
			}

			var el_key = _h.parseHtml("<input type=\"hidden\" name=\"" + name + ".Key\" class=\"selected-token-key\" value=\"" + item.value + "\" />");
			var el_value = _h.parseHtml("<input type=\"hidden\" name=\"" + name + ".Value\" class=\"selected-token-value\" value=\"" + item.text + "\" />");

			// create selected value list item

			var el_title = document.createElement("p");
			if(_settings.renderSelectedValueAsHTML) {
				el_title.innerHTML = item.label || item.text;
			} else {
				el_title.innerText = item.label || item.text;
			}

			var el_remove = _h.parseHtml("<div class=\"clear-value\">" + _const.defaultIcons.x + "</div>");

			var el_li = document.createElement("li");
			el_li.appendChild(el_title);
			el_li.appendChild(el_remove);

			el_li.appendChild(el_key);
			el_li.appendChild(el_value);

			el_li.xelectItem = item;
			el_li.ref_key = el_key;
			el_li.ref_val = el_value;

			item.elSelectedValue = el_li;

			if (typeof _settings.validation === "function") {
				var isValid = _settings.validation(item.text);
				if (!isValid) {
					_h.addClass(el_li, "xelect-invalid");
				}
			}

			_h.on(el_remove, "click", function (e) {
				e.stopPropagation();

				if (_disabled || _h.hasClass(el_li, "disabled")) {
					return;
				}

				removeValue(el_li);
			});

			return el_li;
		}

		function acDefaultRenderItem(i, q) {
			var highlightedText = _h.highlight(i.displayName, q);
			return "<li key='" + i.id + "' class='" + (i.disabled ? "disabled" : "") + "'>" + highlightedText + "</li>";
		}

		function defaultRenderItem(data) {

			var classList = [];
			if (data.selected) {
				classList.push("selected");
			}
			if (data.disabled) {
				classList.push("disabled");
			}
      if (data.isGroup) {
          classList.push("group");
      }

			return "<li key='" + data.value + "' class='" + (classList.join(" ")) + "'>" +
				"<div class=\"original-text-container\">" +
					"<span class=\"original-text highlightable\">" + data.text + "</span>" +
				"</div>" +
				"<div class=\"highlighted-text\"></div>" +
			"</li>";
		}

		/**
		 * Set element as active option
		 * @param {XelectItem} item
		 */
		 function setActiveOption(item) {
			if (_activeOption) {
				_activeOption.active = false;
			}

			_activeOption = item;
			if (_activeOption) {
				_activeOption.active = true;
			}
		}

		/**
		 * Set element as active selected value
		 * @param {HTMLLIElement} el_li
		 */
		function setActiveSelectedValue(el_li) {
			if (_activeSelectedValue) {
				_h.removeClass(_activeSelectedValue, "active");
			}
			_activeSelectedValue = el_li;
			if (_activeSelectedValue) {
				_h.addClass(_activeSelectedValue, "active");
			}
		}

		/**
		 *
		 * @param {Array} availableItems
		 */
		function calcMaxHeight(availableItems) {

			var maxHeight = _settings.maxHeight;

			var availableItemsCount = availableItems.length;

			if (availableItemsCount === 0) {
				// when there are no items available, show little empty space
				if (!_isAutocomplete) {
					_h.css(_el_optionsList, {
						height: "17px",
						overflow: "auto"
					});
				}
			} else if (maxHeight && availableItemsCount > maxHeight) {
				// when available items count exceeds max items to show, add scroll
				var h = _h.height(availableItems[0]);
				_h.css(_el_optionsList, {
					height: (h * maxHeight) + "px",
					"overflow-y": "scroll"
				});
				_el_optionsList.scrollTop = 0;
			} else if (!maxHeight || availableItemsCount <= maxHeight) {
				_h.css(_el_optionsList, {
					height: "",
					overflow: "auto"
				});
			}

			// screen max
			var rect = _el_optionsList.getBoundingClientRect();
			var mainWin = window;
			while(mainWin !== mainWin.parent) {
				mainWin = mainWin.parent;
			}

			var screenMax = mainWin.innerHeight - parseInt(_el_optionsContainer.style.top);
			var scroll = _h.getWindowScroll(mainWin);
			screenMax += scroll.top;

			if (rect.height > screenMax) {
				_h.css(_el_optionsList, {
					height: screenMax + "px",
					overflow: "auto"
				});
			}
		}

		/**
		 *
		 */
		function getLastSelectedValue() {
			return _h.getChild(_el_selectedValuesList, "li:not(.disabled):last-child");
		}

		/**
		 *
		 */
		function getNextSelectedValue(li) {

			// todo - return _h.next(li, ":not(.disabled)");
			var ix = _h.indexOf(li) + 2;
			return _h.getChild(_el_selectedValuesList, ":nth-child(n+" + ix + "):not(.disabled)");
		}

		/**
		 *
		 */
		function getPrevSelectedValue(li) {

			var ix = _h.indexOf(li);
			var li_prevSelectedValue = _h.getChildren(_el_selectedValuesList, ":nth-child(-n+" + ix + "):not(.disabled)");
			if (li_prevSelectedValue) {
				li_prevSelectedValue = li_prevSelectedValue[li_prevSelectedValue.length - 1];
			}

			return li_prevSelectedValue;
		}

		/**
		 *
		 */
		function getFirstOption() {
			return _h.getChild(_el_optionsList, ":not(.selected):not(.no-match):not(.disabled):not(.group)");
		}

		/**
		 *
		 */
		function getNextOption(li) {

			var ix = _h.indexOf(li) + 2;
			return _h.getChild(_el_optionsList, ":nth-child(n+" + ix + "):not(.selected):not(.no-match):not(.disabled):not(.group)");
		}

		/**
		 *
		 */
		function getPrevOption(li) {
			var ix = _h.indexOf(li);
			var prevLi = _h.getChildren(_el_optionsList, ":nth-child(-n+" + ix + "):not(.selected):not(.no-match):not(.disabled):not(.group)");
			if (prevLi) {
				prevLi = prevLi[prevLi.length - 1];
			}

			return prevLi;
		}

		/**
		 * Remove multiselect selected value
		 * @param {HTMLLIElement} el_li
		 */
		function removeValue(el_li) {

			var item = el_li.xelectItem;
			item.selected = false;

			if (_activeSelectedValue === el_li) {

				var next = getNextSelectedValue(_activeSelectedValue);
				setActiveSelectedValue(next);
			}

			_selectedValues = grep(_selectedValues, function (x) {
				return x.id !== item.id;
			});

			_h.remove(el_li.ref_key);
			_h.remove(el_li.ref_val);
			_h.remove(el_li);
			
			// recalc indexes
			var selectedValues = _this.getSelectedValues();
			for (var i = 0; i < selectedValues.length; i++) {

				var elSelectedValue = selectedValues[i].elSelectedValue;

				var refKey = elSelectedValue.ref_key;
				refKey.setAttribute("name", refKey.name.replace(/\[(\d+)\](\.Key$)/, "[" + i + "]$2"))

				var refVal = elSelectedValue.ref_val;
				refVal.setAttribute("name", refVal.name.replace(/\[(\d+)\](\.Value$)/, "[" + i + "]$2"))
			}

			var availableItems = _h.getChildren(_el_optionsList, ":not(.selected)");
			calcMaxHeight(availableItems);

			_this.publish("valueRemoved", item);
			_this.change();
		}

		/**
		 * Add given xelect item to selected values
		 * @param {XelectItem} item - available for autocomplete items
		 * @returns {bool}
		 */
		function selectItem(item) {

			if (_this.publish("beforeSelect", item) === false) {
				return false;
			}

			if (item.disabled || item.isGroup) {
				return false;
			}

			if (item.selected) {
				// item was already selected
				// prevent duplicate
				_this.close();
				return false;
			}

			if (_isMultiselect) {

				item.selected = true;
				var el_li = _settings.renderSelectedValue(item);

				_el_selectedValuesList.appendChild(el_li);

				_selectedValues.push(item);

				calcMaxHeight(_el_optionsList.querySelectorAll("li:not(.selected)"));

				if (_isAutocomplete) {
					_el_optionsList.innerHTML = "";
					calcMaxHeight([]);
				}

			} else {
				// single-select

				if (_selectedValue) {
					_selectedValue.selected = false;
				}

				_selectedValue = item;
				_selectedValue.selected = true;

				_el_textInput.value = item.label || item.text;
				_el_valueInput.value = item.value;

				_h.addClass(_el_xelectTitle, "has-value");
			}

			_this.close();
			_this.publish("select", item);
			_this.change();

			return false;
		}

		/**
		 * Functionality to navigate between options and select them.
		 * Also navigating beween selected values and removing them.
		 * @param {event} e
		 * @returns {}
		 */
		function keyDown(e) {
			var code = e.which;

			if (_isCombobox) {
				var isDelimiter = _settings.delimiters.indexOf(code) >= 0;
				if (isDelimiter) {

					if (!_el_textInput.value && (code === _const.KEY.ENTER || code === _const.KEY.NUMPAD_ENTER || code === _const.KEY.TAB)) {
						// continue
					}
					else {
						// create new token
						e.preventDefault();
						e.stopPropagation();

						_this.close();
						_this.open();

						return;
					}
				}

				switch (code) {
					case _const.KEY.ESCAPE:
						_this.clear();
						break;
				}
			}

			switch (code) {
				case _const.KEY.LEFT:

					if (_isCombobox || _isAutocomplete || _isDropdown) {
						return true;
					}

				case _const.KEY.UP:

					var prevLi;
					if (_activeOption) {
						prevLi = getPrevOption(_activeOption.elOption);
					}

					if (prevLi) {
						var prevTop = _h.position(prevLi).top + _h.height(prevLi) - _el_optionsList.scrollTop;
						if (prevTop <= 0) {
							_el_optionsList.scrollTop -= _h.height(prevLi);
						}
						setActiveOption(prevLi.xelectItem);
					} 
					else if (_isMultiselect) {

						// navigate selected values
						setActiveOption(undefined);

						var li_prevSelectedValue = _activeSelectedValue ?
							getPrevSelectedValue(_activeSelectedValue) :
							getLastSelectedValue();

						if (li_prevSelectedValue) {
							setActiveSelectedValue(li_prevSelectedValue);
						}
					}

					if (_isCombobox || _isAutocomplete || _isDropdown) {
						e.preventDefault();
						return false;
					}

					break;

				case _const.KEY.RIGHT:

					if (_isCombobox || _isAutocomplete || _isDropdown) {
						return true;
					}

				case _const.KEY.DOWN:

					if (_activeSelectedValue) {

						var li_nextSelectedValue = getNextSelectedValue(_activeSelectedValue);
						setActiveSelectedValue(li_nextSelectedValue);

					} else {

						if (!_isOpened) {
							_this.open();
						}

						var nextLi = _activeOption ?
							getNextOption(_activeOption.elOption) :
							getFirstOption();

						if (nextLi) {
							var nextTop = _h.position(nextLi).top;
							var maxTop = _h.height(_el_optionsList);
							if (nextTop >= maxTop) {
								_el_optionsList.scrollTop += _h.height(nextLi);
							}
							setActiveOption(nextLi.xelectItem);
						}
					}

					if (_isCombobox || _isAutocomplete || _isDropdown) {
						e.preventDefault();
						return false;
					}

					break;

				case _const.KEY.HOME:

					// if (!_isOpened) {
					//     _this.open();
					// }

					// var $first = _$ulOptions.children(":not(.selected):not(.no-match):not(.disabled):first");
					// _setActiveOption($first);

					break;

				case _const.KEY.END:

					// if (!_isOpened) {
					//     _this.open();
					// }

					// var $last = _$ulOptions.children(":not(.selected):not(.no-match):not(.disabled):last");
					// _setActiveOption($last);

					break;

				case _const.KEY.PAGE_UP:
					// todo - KEY.PAGE_UP
					break;

				case _const.KEY.PAGE_DOWN:
					// todo - KEY.PAGE_DOWN
					break;

				case _const.KEY.TAB:
				case _const.KEY.ESCAPE:
					if (!_isCombobox) {

						setActiveSelectedValue(undefined);

						_this.close();
					}
					break;

				case _const.KEY.NUMPAD_ENTER:
				case _const.KEY.ENTER:

					if (_activeOption && !_isCombobox) {
						selectItem(_activeOption);
						e.preventDefault();
					}
					else if (_isMultiselect && !_activeOption && _settings.forceValue === false) {

						var val = _el_textInput.value;
						_this.addOptions([
							{value: val, text: val, selected: true}
						]);

						_this.close();
						e.preventDefault();
					}

					// else forms will be submitted

					break;
			}

			if (_isMultiselect) {
				if (_activeSelectedValue) {
					switch (code) {

						case _const.KEY.BACKSPACE:
						case _const.KEY.DELETE:
						case _const.KEY.SPACE:

							removeValue(_activeSelectedValue);
							e.preventDefault();
							break;

						default:

							setActiveSelectedValue(undefined);
							break;
					}
				}
			}

			return true;
		}

		/**
		 * Filtering
		 * @param {} e
		 * @returns {}
		 */
		function keyUp(e) {
			var code = e.which;

			// just navigating
			if (code == _const.KEY.DOWN || code == _const.KEY.UP || code == _const.KEY.LEFT || code == _const.KEY.RIGHT || code == _const.KEY.PAGE_DOWN ||
				code == _const.KEY.PAGE_UP || code == _const.KEY.ESCAPE || code == _const.KEY.ENTER || code == _const.KEY.NUMPAD_ENTER ||
				code == _const.KEY.TAB || code == _const.KEY.SHIFT) {
				return;
			}

			var text = this.value;

			var isLetter = code >= 65 && code <= 90;
			var isNumber = code >= 48 && code <= 57;
			var isNumPad = code >= 96 && code <= 105;

			var specialChars = [_const.KEY.BACKSPACE, _const.KEY.DELETE, 32, 106, 107, 109, 110, 111, 186, 187, 188, 189, 190, 191, 192, 219, 220, 221, 222, 226];
			var isSpecial = specialChars.indexOf(code) >= 0;

			// user has changed input text. reset navigation with keyboard
			if (_prevText !== text) {
				setActiveOption(undefined);
				if (!_settings.forceValue) {
					if (!_isMultiselect && _selectedValue) {
						_selectedValue.selected = false;
						_selectedValue = undefined;
					}
					_el_valueInput.value = text;
				}
			}
			_prevText = text;

			if (_isCombobox || _isDropdown) {
				if (!_isOpened && (isLetter || isNumber || isNumPad || isSpecial)) {
					_this.open();
				}

				filter(
					function (item) {
						var ok = item.checkHighlight(text);
						if (!ok) {
							item.clearHighlight();
						}
						return ok
					},
					function (item) {
						_h.removeClass(item.elOption, "no-match");
					},
					function (item) {
						_h.addClass(item.elOption, "no-match");
					});

				calcMaxHeight(_el_optionsList.querySelectorAll("li:not(.no-match)"));
			}

			if (_isAutocomplete) {

				if (code !== _const.KEY.DELETE && code !== _const.KEY.BACKSPACE && !isLetter && !isNumber && !isNumPad && !isSpecial) {
					return;
				}

				if (!_isOpened) {
					_this.open();
				}

				setActiveOption(undefined);

				_el_optionsList.innerHTML = "";
				_h.hide(_el_optionsList);

				_clearAcTimeout();

				if (text === "") {

					_this.close();
					_this.open();
					return;
				}

				showHelpText(_settings.labels.searching);

				_execAutocompleteRequestTimeout = setTimeout(function () {

					var data = {};
					var key = _settings.ac.queryKey;
					data[key] = text;
					
					var excludeIdsIterator = (function excludeIds() {
						var selectedValues = __("input.selected-token-key", _el_selectedValuesList);

						for (var i = 0; i < selectedValues.length; i++) {
							var selectedValue = selectedValues[i];
							data["excludeIds[" + i + "]"] = selectedValue.value;
						}

						return i;
					})();

					(function remoteData(excludeIdsIterator) {
						if (typeof _settings.ac.dataFunc === "function") {

							var additionalData = _settings.ac.dataFunc.call(_this);
							for (var key in additionalData) {
								if (additionalData.hasOwnProperty(key)) {

									if (key === "excludeIds" && Array.isArray(additionalData[key])) {
										for (var i = 0; i < additionalData[key].length; i++) {
											var excludeId = additionalData[key][i];
											data["excludeIds[" + (i + excludeIdsIterator) + "]"] = excludeId;
										}
									}
									else {
										data[key] = additionalData[key];
									}
								}
							}
						}
					})(excludeIdsIterator);

					if (_settings.ac.src) {
						var res = [];
						for (var i = 0; i < _settings.ac.src.length; i++) {
							if (_settings.ac.match(_settings.ac.src[i], text)) {
								res.push(_settings.ac.src[i]);
							}
						}

						_settings.ac.sort(res, text);
						if (res.length > _settings.ac.limit) {
							res = res.slice(0, _settings.ac.limit);
						}

						handleAcResult(res);
					}
					else {

						var url = _settings.ac.url;
						if (_settings.ac.method.toLowerCase() == "get") {
							url = _urlHelper.addQueryString(url, data);
						}

						if (_settings.ac.crossDomain) {
							// JSONP
							var callbackName = 'jsonp_' + Math.round(100000 * Math.random());
							window[callbackName] = function (data) {
								delete window[callbackName];
								document.body.removeChild(script);
								handleAcResult(data);
							};

							var script = document.createElement('script');
							script.src = _urlHelper.addQueryString(url, { callback: callbackName });
							
							document.body.appendChild(script);
						}
						else {
							_acXhr = Ajax(_settings.ac.url, {
									method: _settings.ac.method,
									contentType: _settings.ac.contentType,
									dataType: _settings.ac.dataType,
									data: data
								})
								.done(function (res) {
                    _acXhr = null;
                    handleAcResult(res);
                });
						}
					}

					function handleAcResult(res) {

						if (typeof _settings.ac.transformResult == "function") {
							res = _settings.ac.transformResult(res);
						}

						if (res.length === 0) {
							showHelpText(_settings.labels.noResults);
							return;
						}

						hideHelpText();

						for (var i = 0; i < res.length; i++) {
							(function (itemData) {
								itemData.text = itemData.text || itemData.displayName || itemData.name;
								itemData.value = itemData.value || itemData.id;
								var liHtml = _h.parseHtml(_settings.ac.renderItem(itemData, text));
								var item = new XelectItem(liHtml, itemData);

								_el_optionsList.appendChild(liHtml);
								liHtml.addEventListener("mousedown", function (e) {
									e.preventDefault();
									selectItem.call(liHtml, item);
								});
								liHtml.addEventListener("mouseenter", function () {
									setActiveOption(item);
								});
							})(res[i]);
						}

						_h.show(_el_optionsList);

						if (_isMultiselect) {
							calcMaxHeight(_el_optionsList.querySelectorAll("li:not(.selected)"));
						} else {
							calcMaxHeight(_el_optionsList.querySelectorAll("li"));
						}
					}

				}, 300);
			}
		}

		function filter(test, match, noMatch) {

			var groups = {};
			
			// todo - optimize filtering
			// store matched items to local collections and use them instead of iterating all items all the time
			for (var i = 0; i < _options.length; i++) {
				var item = _options[i];
				if (item.isGroup) {
					var groupId = item.value;
					groups[groupId] = {
						item: item,
						children: []
					};

					if (test(item)) {
						groups[groupId].matchGroupName = true;
					}

					continue;
				}

				var parentGroup = null;
				if (item.data.group && groups[item.data.group]) {
					parentGroup = groups[item.data.group];
					parentGroup.children.push(item);
				}

				if (!test(item)) {
					noMatch(item);
				} else {
					match(item);
					if (item.data.group && groups[item.data.group]) {
						groups[item.data.group].show = true;
					}
				}
			}

			for(var groupName in groups) {
				if (groups.hasOwnProperty(groupName)) {

					var g = groups[groupName];
					if(g.show) {

						while (g) {
							match(g.item);
							g = g.item.data.group ? groups[g.item.data.group] : null;
						}
					}
					else if (g.matchGroupName) {
						var g_temp = g;
						while (g_temp) {
							match(g_temp.item);
							g_temp = g_temp.item.data.group ? groups[g_temp.item.data.group] : null;
						}

						for (var i = 0; i < g.children.length; i++) {
							match(g.children[i]);
						}
					}
					else {
						noMatch(g.item);
					}
				}
			}
		}

		function _clearAcTimeout() {

			clearTimeout(_execAutocompleteRequestTimeout);
			if (_acXhr) {
				_acXhr.abort();
				_acXhr = null;
			}
		}

		function bindSingleOptionEvents(item) {
			item.elOption.addEventListener("mousedown", function (e) {
				// keep focus on text input
				e.preventDefault();
			});
			item.elOption.addEventListener("click", function () {
				selectItem(item);
			});
			item.elOption.addEventListener("mouseenter", function () {
				setActiveOption(item);
			});
		}

		function createComboboxItem() {
			var itemKey = _el_textInput.value;
			var inputValue = _el_textInput.value;
			if (_settings.defaultKey) {
				itemKey = _settings.defaultKey;
			}

			var options = addOptions([{ text: inputValue, value: itemKey, selected: true }]);
			renderOptions(options);

			if (_isMultiselect) {
				_el_textInput.value = "";
			}
		}

		/**
		 * Add new option to xelect element
		 * @param {Array} options
		 *
		 */
		function addOptions(options) {

			if (!options) {
				return [];
			}

			var createdItems = [];
			for (var i = 0, len = options.length; i < len; i++) {
				var x = options[i];

				var item;

				if (x instanceof XelectItem) {
					item = x;
				}
				else if (typeof x === "string") {
					item = createXelectItem({ text: x, value: x});
				} else {

					if (!x.hasOwnProperty("value")) {
						var keys = Object.keys(x);
						var value = keys[0];
						var data = {
							value: value,
							text: x[value]
						};

						for (var ki = 0; ki < keys.length; ki++) {
							var key = keys[ki];
							var val = x[key];
							data[key] = val;
						}

						x = data;
					}

					item = createXelectItem(x);
				}

				addOption(item);
				createdItems.push(item);
			}

			if (!_isMultiselect && _settings.allowEmpty === false && !_selectedValue && createdItems.length > 0) {
				createdItems[0].selected = true;
				_selectedValue = createdItems[0];
			}

			return createdItems;
		}

		function renderOptions(items){

			if (!items) {
				return;
			}
			for (var i = 0, len = items.length; i < len; i++) {

				var xelectItem = items[i];
				_el_optionsList.appendChild(xelectItem.elOption);

				if (xelectItem.selected) {
					if (_isMultiselect) {
						var li = _settings.renderSelectedValue(xelectItem, _settings.name);
						_el_selectedValuesList.appendChild(li);
					} else {
						_el_textInput.value = xelectItem.label || xelectItem.text;
						_el_valueInput.value = xelectItem.value;
						_h.addClass(_el_xelectTitle, "has-value");
					}
				}

				_this.publish("valueAdded", xelectItem);
			}
		}

		/** Add item to options list. If option item is selected it will added to selected items */
		function addOption(item) {
			_options.push(item);
			_optionsDict[item.value] = item;

			if (item.selected) {
				if (_isMultiselect) {
					_selectedValues.push(item);
				}
				else {
					_selectedValue = item;
				}
			}
		}

		function createXelectItem(data) {
			var html = _settings.renderItem(data);
			var el = _h.parseHtml(html);
			return new XelectItem(el, data);
		}

		function onScroll(e) {
			// Close xelect on scroll, same behaviour as default select element.
			// Avoids problems in iframe when xelect moves relative to
			// the source element or leaves the iframe bounds.
			
			var shouldClose;

			if (e.target === _lastScrollTarget) {

				shouldClose = false;
			}
			else {
				
				// check if element scrolled was xelect element
				var el = null;
				if (e.target.closest) {
					el = e.target.closest(".xelect-options");
				}
				shouldClose = el === null || el === undefined;
			}
	
			_lastScrollTarget = e.target;
	
			// close when scroll was used out of xelect area
			if (shouldClose) {
				console.log("scroll close xelect");
				_this.close();
			}
			else {
				var ul = e.target.closest("ul");
				if (ul) {
					// i.e on options

					var h = parseInt(ul.style.height);
					if (!h || h == ul.scrollHeight) {
						// is not scrollable
						e.preventDefault();
						return;
					}

					var down = e.deltaY > 0;
					var up = e.deltaY < 0;

					var scrollTop = ul.scrollTop
					if ((down && scrollTop + parseInt(ul.style.height) >= ul.scrollHeight) || (up && scrollTop <= 0)) {
						// is scrolling
						e.preventDefault();
					}
				}
			}
		}

		__construct.apply(this, [].slice.call(arguments));
	};

	Xelect.change = _static.change;

	/**
	 * initialize existing xelects
	 */
	Xelect.init = function (selector, settings) {

		var res = [];

		var elements = document.querySelectorAll(selector);
		for (var i = 0; i < elements.length; i++) {
			var el = elements[i];
			var xelect = new Xelect(el, settings);
			res.push(xelect);
		}
		return res;
	};

	Xelect.get = function (id) {
		return Xelect(id);
	}

	function getInlineSettings(el) {
		return {
			id: el.id,
			name: el.getAttribute("name"),
			multiselect: _h.getAttrAsBoolean(el, "data-xelect-multiselect") || el.classList.contains("xelect-multiselect"),
			forceValue: _h.getAttrAsBoolean(el, "data-xelect-force-value"),
			allowEmpty: _h.getAttrAsBoolean(el, "data-xelect-allow-empty"),
			type: el.getAttribute("data-xelect-type"),
			src: JSON.parse(el.getAttribute("data-xelect-src")),
			maxHeight: _h.getAttrAsInt(el, "data-xelect-max-height"),
			readOnly: JSON.parse(el.getAttribute("data-xelect-readonly")),
			placeholder: el.getAttribute("placeholder"),
			ac: {
				url: el.getAttribute("data-xelect-ac-url"),
				method: el.getAttribute("data-xelect-ac-method"),
				queryKey: el.getAttribute("data-xelect-ac-query-key"),
				crossDomain: _h.getAttrAsBoolean(el, "data-xelect-ac-cross-domain"),
				dataType: el.getAttribute("data-xelect-ac-data-type"),
				contentType: el.getAttribute("data-xelect-ac-content-type")
			}
		};
	}

	/**
	 *
	 */
	function buildSettings(settings, inlineSettings, defaults) {
		settings = settings || {};
		inlineSettings = inlineSettings || {};
		defaults = defaults || _defaults;

		var res = {};
		for (var i in defaults) {
			if (defaults.hasOwnProperty(i)) {

				if (typeof defaults[i] === "object" && defaults[i] !== null) {
					if (Array.isArray(defaults[i])) {
						res[i] = settings[i] || defaults[i];
					}
					else {
						res[i] = buildSettings(settings[i], inlineSettings[i], defaults[i]);
					}
				} else {
					res[i] = coalesce(settings[i], inlineSettings[i], defaults[i]);
				}
			}
		}

		return res;
	}

	function overrideDefaults(newDefaults, defaults) {

		for (var i in newDefaults) {
			if (newDefaults.hasOwnProperty(i) && defaults.hasOwnProperty(i)) {
				if (typeof defaults[i] === "object") {
					overrideDefaults(newDefaults[i], defaults[i]);
				} else {
					defaults[i] = newDefaults[i];
				}
			}
		}
	}

	function grep(items, callback) {
		var filtered = [],
			len = items.length,
			i = 0;
		for (i; i < len; i++) {
			var item = items[i];
			var cond = callback(item);
			if (cond) {
				filtered.push(item);
			}
		}

		return filtered;
	}

	function coalesce() {

		for (var i = 0; i < arguments.length; i++) {
			var arg = arguments[i];
			if (arg !== null && typeof (arg) !== "undefined") {
				return arg;
			}
		}

		return null;
	}

	function applyStyles(el_xelectInput, el_options){
		if(!el_xelectInput || !el_options){
			return;
		}

		var styles = window.getComputedStyle(el_xelectInput);
		var boundingRect = el_xelectInput.getBoundingClientRect();

		var padding = {
			top: parseFloat(styles.paddingTop),
			left: parseFloat(styles.paddingLeft),
			right: parseFloat(styles.paddingRight)
		};

		_h.css(el_options, {
			top: boundingRect.top + padding.top + boundingRect.height + window.pageYOffset + "px",
			left: boundingRect.left + padding.left + window.pageXOffset + "px",
			width: boundingRect.width - padding.left + "px",
		});

	}

	Xelect.overrideDefaults = function (newDefaults) {
		overrideDefaults(newDefaults, _defaults);
	};

	ns.Xelect = Xelect;
	ns.XelectItem = XelectItem;
	ns.XelectItemView = XelectItemView;

})(sparkling, window);