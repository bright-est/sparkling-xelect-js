interface IXelectSettings {
    id?: string,
    type?: string,
    name?: string
    multiselect?: boolean,
    forceValue?: boolean,
    allowEmpty?: boolean,
    // type: _const.XELECT_TYPE.SELECT,
    src?: IXelectItemData[],
    maxHeight?: number,

    /**
     * Token input value validator
     */
    validation?: (text: string) => boolean,
    
    placeholder?: string,
    labels?: {
        hint?: string,
        searching?: string,
        noResults?: string
    },
    disabled?: boolean,
    // event handlers
    change?: (selectedItem: XelectItem | XelectItem[]) => any
    beforeSelect?: (item: XelectItem) => any,
    select?: (item: XelectItem) => any,
    beforeClear?: () => any,
    clear?: () => any,
    defaultKey?: string,
    delimiters?: string[],
    ac?: {
        url?: string,
        method?: "POST" | "GET",
        queryKey?: string,
        crossDomain?: boolean,
        dataType?: string,
        contentType?: string,
        transformResult?: (res: any) => any[],
        dataFunc?: () => any,
        src?: string[],
        match?: (value:string, term: string) => boolean,
        sort?: (arr: any[], term: string) => void,
        limit?: number,
    },
	readOnly?: boolean,
	renderSelectedValueAsHTML?: boolean
}

declare class XelectItem {

    constructor(el: HTMLElement, data?: IXelectItemData);

    id: string;
    elOption: HTMLElement;
    data: any;

    elSelectedValue(): HTMLElement;
    elSelectedValue(el: HTMLElement): void;

    selected: boolean;
    disabled: boolean;
    isGroup: boolean;
    active: boolean;
    value: string;
    text: string;
    label: string;

    show(): void;
    hide(): void;
}

declare class Xelect {
    constructor(el: string | HTMLElement, settings?: IXelectSettings);

    updateSettings(settings: IXelectSettings): void;

    open(): void;
    close(): void;
    clear(): boolean;
    disable(): void;
    enable(enable?: boolean): void;

    selectValue(value: string): boolean;
    selectItem(item: XelectItem): boolean;

    getOptions(): XelectItem[];
    addOption(option: XelectItem): void;
    addOptions(options: IXelectItemData[]): void;
    removeOption(item: XelectItem): void;
    clearOptions(): void;
    
    getSelectedItem(): XelectItem;
    getSelectedItems(): XelectItem[];

    removeSelectedItem(value: string | XelectItem): void;
    removeSelectedItems(array: (string | XelectItem)[]): void;

    change(handler: (selectedItem: XelectItem) => any): void;
    change(handler: (selectedItems: XelectItem[]) => any): void;

    beforeSelect(handler: (item: XelectItem) => any): Xelect;
    onSelect(handler: (item: XelectItem) => any): Xelect;

    beforeClear(handler: () => any): Xelect;
    onClear(handler: () => any): Xelect;

    beforeClose(handler: () => any): Xelect;
    onClose(handler: () => any): Xelect;

    filter(test: (item: XelectItem) => boolean, match: (item: XelectItem) => any, noMatch: (item: XelectItem) => any): void;

    static get(id: string): Xelect;
    static init(selector: string, settings?: IXelectSettings): void;
}

declare interface IXelectItemData {
    text?: string,
    value?: string,
    label?: string,
    selected?: boolean,
    disabled?: boolean,
    isGroup?: boolean,
    data?: any
}