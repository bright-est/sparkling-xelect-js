(function regexHelper() {

	var regexpSpecialChars = new RegExp('[.\\\\+*?\\[\\^\\]$(){}=!<>|:\\-]', 'g');

	this.escape = function (term) {
		if (!term) {
			return term;
		}
		return term.replace(regexpSpecialChars, '\\$&');
	};

	return this;
})();