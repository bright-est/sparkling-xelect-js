(function (){

	var _xelectItemCounter = 0;
	var _xelectItemStatics = {};
	sparkling.Observable.mixin(_xelectItemStatics);

	function XelectItem() {

		var _this = this;
		sparkling.Observable.mixin(this);

		var _id = _xelectItemCounter++;
		var _elOption;
		var _elSelectedValue;
		var _itext;
		var _view;
		var _data;

		Object.defineProperties(this, {
			"id": {
				get: function () { return _id; }
			},
			"elOption": {
				get: function () { return _elOption; }
			},
			"data": {
				get: function () { return _data; }
			},
			"elSelectedValue": {
				get: function () { return _elSelectedValue; },
				set: function (val) { _elSelectedValue = val; }
			}
		});

		var _backingFields = {
			selected: false,
			disabled: false,
			visible: true,
			isGroup: false,
			active: false,
			value: undefined,
			text: undefined,
			label: undefined
		};

		this.defineObservableProperties(_backingFields);

		function __construct(liElement, o) {

			o = o || {};

			_backingFields.value = o.value;
			_backingFields.text = o.text;
			_backingFields.label = o.label;
			_backingFields.selected = o.selected === true;
			_backingFields.disabled = o.disabled === true;
			_backingFields.isGroup = o.isGroup === true;
			_backingFields.visible = o.visible !== false;

			_data = o.data || o;
			_itext = (o.text || "").toUpperCase();

			_this.propertyChanged("text", function (val) {
				if (typeof val === "string") {
					_itext = val.toUpperCase();
				}
			});

			if (liElement) {
				_elOption = liElement;
				_elOption.xelectItem = this;
				_view = new XelectItemView(_elOption, _this);
			}

			_xelectItemStatics.publish("created", _this);
		}

		/**
		 * Publish highlight event if match
		 * @return {bool}
		 */
		this.highlight = this.createPublishSubscribe("highlight");
		this.clearHighlight = this.createPublishSubscribe("clearHighlight");

		this.checkHighlight = function (term) {
			if (_this.match(term)) {
				_this.highlight(_this.text, term);
				return true;
			}
			return false;
		};

		/**
		 * Check if item matches given term
		 */
		this.match = function (term) {
			return _itext.indexOf(term.toUpperCase()) >= 0;
		};

		// function createItemElement() {
		// 	// todo - this stuff needs to be in view
		// 	return _h.parseHtml("<li key='" + _backingFields.value + "' class='" + (_backingFields.selected ? "selected" : "") + "'>" +
		// 			"<span class=\"original-text\">" + _backingFields.text + "</span>" +
		// 			"<span class=\"highlighted-text\"></span>" +
		// 			"</li>");
		// }

		this.show = function() {
			this.disabled = false;
			_elOption.classList.remove("xelect-option--hidden");
		}

		this.hide = function() {
			this.disabled = true;
			_elOption.classList.add("xelect-option--hidden");
		}

		__construct.apply(this, [].slice.call(arguments));
	}

	XelectItem.created = function (handler) {
		_xelectItemStatics.subscribe("created", handler);
	};

	return XelectItem;
})();
