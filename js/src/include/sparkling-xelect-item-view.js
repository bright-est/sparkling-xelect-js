function XelectItemView(liElement, xelectItem) {

	var _el_option = liElement;

	var _el_originalTextContainer = _(".original-text-container", _el_option);
	var _el_originalText = _(".original-text", _el_option);
	var _el_highlightedText = _(".highlighted-text", _el_option);

	xelectItem
		.propertyChanged("value", function (val) {
			_el_option.setAttribute("key", val);
		})
		.propertyChanged("text", function (val) {
			_el_originalText.innerHTML = val;
		})
		.propertyChanged("selected", function (val) {
			if (val) {
				_h.addClass(_el_option, "selected");
			}
			else {
				_h.removeClass(_el_option, "selected");
			}
		})
		.propertyChanged("disabled", function (val) {
			if (val) {
				_h.addClass(_el_option, "disabled");
			}
			else {
				_h.removeClass(_el_option, "disabled");
			}
		})
		.propertyChanged("active", function (val) {
			if (val) {
				_h.addClass(_el_option, "hover");
			}
			else {
				_h.removeClass(_el_option, "hover");
			}
		})
		.propertyChanged("isGroup", function (val) {
			if (val) {
				_h.addClass(_el_option, "group");
			}
			else {
				_h.removeClass(_el_option, "group");
			}
		})
		.highlight(function (text, term) {
			_h.addClass(_el_originalTextContainer, "original-text-hidden");

			var clone = _el_originalTextContainer.cloneNode(true);
			var highlightables = __(".highlightable", clone);
			for (var i = 0; i < highlightables.length; i++) {
				highlightables[i].innerHTML = _h.highlight(highlightables[i].innerText, term);
			}

			_el_highlightedText.innerHTML = "";
			while (clone.firstChild) {
				_el_highlightedText.appendChild(clone.firstChild);
			}

		})
		.clearHighlight(function () {
			_h.removeClass(_el_originalTextContainer, "original-text-hidden");
			_el_highlightedText.innerHTML = "";
		});
}