(function htmlHelper() {

	var _this = this;

	/**
	 * Add css class to an element
	 */
	this.addClass = function (el, className) {
		if (el.classList) {
			el.classList.add(className);
		}
		else {
			if (!_this.hasClass(el, className)) {
				el.className += " " + className;
			}
		}
	};

	/**
	 * Remove css class from an element
	 */
	this.removeClass = function (el, className) {
		if (el.classList) {
			el.classList.remove(className);
		}
		else {
			el.className = (" " + el.className + " ")
				.replace(/[\n\t]/g, " ")
				.replace(" " + className + " ", "")
				.trim();
		}
	};

	/**
	 * Set inline css styles for an element
	 */
	this.css = function (el, css) {
		for (var i in css) {
			el.style[i] = css[i];
		}
	};

	/**
	 * Check if element has given css class name
	 */
	this.hasClass = function (el, className) {
		return el.classList
			? el.classList.contains(className)
			: (" " + el.className + " ").replace(/[\n\t]/g, " ").indexOf(" " + className + " ") > -1;
	};

	/**
	 *
	 */
	this.hide = function (el) {
		this.css(el, { display: "none" });
	};

	/**
	 *
	 */
	this.show = function (el) {
		this.css(el, { display: "" });
	};

	var count = 0;

	/**
	 * Get direct child elements by selector
	 */
	this.getChildren = function (element, selector) {

		var id = element.id;
		element.id = id || 'query_children_' + count++;
		var attr = '#' + element.id + ' > ';

		selector = attr + (selector + '').replace(',', ',' + attr, 'g');
		var result = element.parentNode.querySelectorAll(selector);
		if (!id) element.removeAttribute('id');
		return result;
	};


	/**
	 * Get single direct child element by selector
	 */
	this.getChild = function (element, selector) {
		var id = element.id;
		element.id = id || 'query_children_' + count++;
		var attr = '#' + element.id + ' > ';

		selector = attr + (selector + '').replace(',', ',' + attr, 'g');
		var result = element.parentNode.querySelector(selector);
		if (!id) element.removeAttribute('id');
		return result;
	};

	/**
	 *
	 */
	this.getSiblings = function (element, selector) {

		var id = element.id;
		element.id = id || 'get_siblings_' + count++;

		var res = _this.getChildren(element.parentNode, selector + ":not(#" + element.id + ")");
		if (!id) element.removeAttribute('id');
		return res;
	};

	/**
	 *
	 */
	this.getSibling = function (el, selector) {

		var siblings = this.getSiblings(el, selector);
		if (siblings && siblings.length > 0) {
			return siblings[0];
		}
		return null;
	};

	/**
	 *
	 */
	this.next = function (element, selector) {

		if (!selector) {
			var el = element;
			while (true) {
				var next = el.nextSibling;
				if (!next) {
					return null;
				}
				// skip text node
				if (next.nodeType !== 3) {
					return next;
				}
				el = next;
			}
		}

		var siblings = _this.getSiblings(element, selector);
		// todo
	};

	/**
	 *
	 */
	this.indexOf = function (node) {
		var index = 0;
		while ((node = node.previousSibling)) {
			if (node.nodeType == 1) {
				index++;
			}
		}
		return index;
	};

	/**
	 *
	 */
	this.on = function (element, eventName, handler) {
		element.addEventListener(eventName, handler);
	};


	/**
	 * Create DOM element from html
	 */
	this.parseHtml = function (html) {
		var tmp = document.createElement("div");
		tmp.innerHTML = html;
		return tmp.children[0];
	};

	/**
	 * Remove element from DOM
	 */
	this.remove = function (el) {
		el.parentNode.removeChild(el);
	};

	/**
	 *
	 */
	this.highlight = function (value, term) {
		if (value == null || !term) {
			return value;
		}

		return value.replace(
			new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + _regexHelper.escape(term) + ")(?![^<>]*>)(?![^&;]+;)", "gi"),
			function (match, p1) {
				if (p1) {
					return "<b>" + p1 + "</b>";
				}
				return "";
			}
		);
	};

	this.offset = function (el) {
		var rect = el.getBoundingClientRect();

		return {
			top: rect.top + document.body.scrollTop,
			left: rect.left + document.body.scrollLeft
		}
	};

	this.width = function (el) {
		return el.offsetWidth;
	};

	this.height = function (el) {
		return el.offsetHeight;
	};

	this.position = function (el) {
		return { left: el.offsetLeft, top: el.offsetTop };
	};

	this.getWindowScroll = function (w) {
		var doc = w.document.documentElement;

		return {
			left: (w.pageXOffset || doc.scrollLeft) - (doc.clientLeft || 0),
			top: (w.pageYOffset || doc.scrollTop) - (doc.clientTop || 0)
		};
	};

	this.getAttrAsBoolean = function (el, attrName) {

		var attrValue = el.getAttribute(attrName);
		if (attrValue) {
			if (attrValue.toLowerCase() === "true" || attrValue === "1") {
				return true;
			}
			if (attrValue.toLowerCase() === "false" || attrValue === "0") {
				return false;
			}
		}
	};

	/**
	 *
	 */
	this.getAttrAsInt = function (el, attrName) {

		var attrValue = el.getAttribute(attrName);
		if (!attrValue) {
			return undefined;
		}

		var res = parseInt(attrValue);
		if (isNaN(res)) {
			return undefined;
		}

		return res;
	};

	return this;
})();

/**
 * Select single element by CSS selector. If multiple elements match selector, first is returned.
 * @param {string} selector
 * @param {HTMLElement} el
 * @returns {HTMLElement}
 */
var _ = function selectSingleElement(selector, el) {
	el = el || document;
	return el.querySelector(selector);
};

/**
 * Select elements by CSS selector.
 * @param {string} selector
 * @param {HTMLElement} el
 * @returns {Array}
 */
var __ = function selectElements(selector, el) {
	el = el || document;
	return el.querySelectorAll(selector);
};