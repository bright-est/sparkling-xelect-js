(function htmlHelper() {
    
    var _this = this;

    this.addQueryString = function(url, data) {

        if (!data) {
            return url;
        }

        var keys = Object.keys(data);
        if (data.keys.length == 0) {
            return url;
        }

        url += (url.indexOf('?') < 0 ? '?' : '&');
        for (var key in keys) {
            url += encodeURIComponent(key) + "=" + encodeURIComponent(data[key]);
        }

        return url;
    }

    return this;
})();