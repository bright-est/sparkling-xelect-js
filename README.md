# xelect.js

Object is to combine inputs such as `Select`, `Combobox`, `Token` `Autocomplete` to single componenet sharing same UX and style with key features like:

* Multiselect
* Values filtering / search

# Install

    npm install @brightspark/sparkling-xelect-js

If you haven't set brightspark registry yet, you should run 

    npm config set @brightspark:registry="https://www.myget.org/F/brightspark/auth/<API key>/npm/"

API key is availible in [our wiki](https://bitbucket.org/bright-est/coding-standards/wiki/Home)

**Note**

Xelect takes advantage of new EcmaScript, therefore older browsers need some sort of polyfill library
for example https://polyfill.io/

## Whats the status?

### common behaviour

[x] blur by pressing ESC  
[ ] 
                
#### select

[x] user can select option only from existing list by:  
  [x] navigating with mouse and by clicking on desired item  
  [x] navigating with arrows and selecting by pressing enter  
[x] user can filter existing list by typing into text field  
[ ] when filtering, different text style must be used for input field, so user could understand it is not a combobox  
[x] if user blurs input field with not existing value, previous value will be restored  
[x] user can not leave text to input field which is not one of the options text.  
[x] user can clear selected value by clicking x  
[ ] disable/enable filtering. If filtering is disabled, control behaves exaclty like default select element  
                
[x] options source can be json-string
[ ] options source can be existing DOM elements
[x] options source can be passed with initialization
            
#### combobox

[ ]

#### autocomplete 

[x]

#### multiselect

[x] can be defined via options with initialization  
[x] can be set by adding class name "xelect-multiselect"  
[X] selected values are shown in separate list  
[x] selected values are not shown in options list  

### Xelect API      

#### Settings

[x] id: string  
[x] name: string  
[x] multiselect: bool  
[x] type: string [combobox | autocomplete | select]  
[x] src: JSONstring | Array  
[ ] Remote datasource  
[x] maxHeight: number - how many items are shown in dropdown  
                

                
#### Methods

[x] open() - focus xelect element  
[x] close() - blur xelect element  
[x] clear() - clear selected value(s)  
[x] disable() - makes element as disabled. no editing is allowed  
[x] enable() - enables editing for xelect element  
[x] change(cb: function()) - publish/subscribe " change" event  
[x] getSelectedValue()  
[x] getSelectedValues()  
[x] removeSelectedValue(value: string|XelectItem) - remove value of selected item
[x] removeSelectedValues(arr: Array) - remove value of all selected items 
[ ] addOption()  
[ ] removeOptions()  
[ ] getOptions()  
[X] selectValue(val) - Select item by value  
[x] selectItem(item) - Select item  
[x] addOption(item) - Register new option for item  
[X] addOptions(options) - Register new options for xelect  
[x] removeOption(item) - Delete item option  
[x] clearOptions() - Delete options list  
[x] showHelpText(text) - make helping text visible  
[x] hideHelpText() - hide helping text  
[x] defaultRenderSelectedValue(item) -  
[x] acDefaultRenderItem(i, q) - default render item for xelect with autocomplete  
[x] defaultRenderItem(data)  
[x] setActiveOption(item) - set item as active option  
[x] setActiveSelectedValue(el_li) - set element as active selected value  
[x] calcMaxHeight(availableItems) - calculate max height for availible items  
[x] getLastSelectedValue() - get last selected value  
[x] getNextSelectedValue(li) - get next selected value  
[x] getPrevSelectedValue(li) - get previous selected value  
[x] getFirstOption() - get first select otion  
[x] getNextOption(li) - get next slect otion  
[x] getPrevOption(li) - get previous select option  
[x] removeValue(el_li) - remove selected value for multiselect  
                
#### Events

[x] "change"  - if xelect value has been changed
[x] "valueRemoved"  - if xelect value has been removed
[x] "beforeSelect"  - before select xelect item
[x] "onSelect"  - if item of xelect has been selected
[x] "beforeClear"  - before value of xelext will be cleared
[x] "clear"  - clear xelct value
[x] "beforeClose" - before blur xelect element

                
#### Properties

[x] isOpened { get; }  
[x] type { get; }  

### XelectItem API  

[x] propertyChanged(handler: function(propName, value, oldValue))  
[x] match(term) - Check if item text matches given term  
[x] highlight(term)   
[x] clearHighlight()  
 

### some day...
- [ ] angular directive
... 

## API

### Getting Started

```
var xelect = new Xelect("x1");
```

Xelect can be launched with certain settings. Settings are passed as JavaScript object:

```
var xelect = new Xelect("x1", {
  multiselect: true,
  src: []
});
```

Source item schema
```
{
  value: 1, 
  text: "C#", 
  selected: true
}
```

#### update settings 

```
var xelect = new Xelect("x1");
xelect.updateSettings({});
```

### Autocomplete


By default server receives two parameters
* q
* excludeIds (when autocomplete is also multiselect)

Server must return json as following:

```
[
  {
    id: "string",
    displayName: "string",
  }
]
```

There are several properties that you can configure for autocomplete:

* url: string - autocomplete data url  
* method: string - http method
* queryKey: string - key used for search term
* crossDomain: bool - use jsonp
* dataType: string -  ajax response datatype
* dataFunc: () => any - function to add additional parameters for ajax request  
* transformResult: (result: any[]) => any[] - function to alter ajax response received from server

To Add some additional excludeIds, you have to use dataFunc

```
myXelect.updateSettings({
  ac: {
    dataFunc: function () {
      return {
        exludeIds: [1, 2, 3]
      }
    }
  }
});
```

### Properties


#### multiselect

Allow user to select multiple values. By default is false

#### type
Define type of xelect field. Type can be select, combobox, autocomplete.

#### src (type:autocomplete)
#### queryKey (type:autocomplete)
#### queryMethod (type:autocomplete)
#### dataType (type:autocomplete)
#### transformResult (type:autocomplete)
#### dataFunc (type:autocomplete)
#### renderItem (type:autocomplete)

#### *ac-hint-text*
#### *ac-searching-text*
#### *ac-no-results-text*

### Events

#### change

#### beforeSelect

If returns false, select will be cancelled

#### onSelect

Subscribe to "select" event. 

```
x.onSelect(function (selectedItem) {
  // 
});
```

#### beforeClear

If returns false, clear will be cancelled

#### onClear

Subscribe to "clear" event.

```
x.onClear(function () {
  // 
});
```

#### *beforeUnselect*
#### *unselect*

#### *itemFocus*


### Methods

#### open
#### close
#### clear

#### disable
#### enable

#### select(item)

Select single item

#### select(items)

Select multiple items.
If xelect is single-select, first item will be selected.

#### unselect(item)

remove single value from selected items list.

#### unselect(items)

remove multiple values from selected items list.
Useful for multi-select xelects.

#### getItems

Returns array of all XelectItems.
*Not avaliable for autocomplete*

#### getSelectedItem

return first selected XelectItem or null. 
Useful for single select xelects

#### getSelectedItems

will return array of selected XelectItems. even if xelect is single-select.
Useful for multi-select xelects.

#### setRenderItem [Obsolete]
Will be replaced with parameter

## jQuery plugin

```
var $xelect = $("#the-xelect, .and-or-any-other-selector").xelect();
```

### Set Propery
#### Setting single property
```
$xelect.xelect("set.propertyName", value); 
```
xelect will return this (i.e $xelect) so method chaining can be used.
#### Setting multiple properties
```
$xelect.xelect("set", {
  x: 1,
  y: 2
);
```
xelect will return this (i.e $xelect) so method chaining can be used.

### Calling method
```
$xelect.xelect("methodName", [arguments])
```

### Subscribing to an event
```
$xelect.xelect("on", "eventName", function handler() {
  // ...
});
```

### Triggering event
```
$xelect.xelect("trigger", "eventName", [arguments]);
```

# Xelect and Vue.js

Vue.js uses some magic attributes. [key](https://vuejs.org/v2/api/#key) is one of them which is also used by Xelect options for storing value.  
To make vue and xelect work together, xelect container must use [v-pre](https://vuejs.org/v2/api/#v-pre) directive (it will tell vue to skip compilation for this element and all its children).


# The team

max
ragnar
tanel
t�nu
aleksei