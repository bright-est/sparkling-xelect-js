properties { 
  
  $signAssemblies = $false
  $buildDocumentation = $false
  $buildNuGet = $true
  $uploadNuGet = $true
  $treatWarningsAsErrors = $false
  $updateVersionNumber = $false

  $baseDir  = resolve-path ..
  $buildDir = "$baseDir\.nuget"
  $sourceDir = "$baseDir"
  $toolsDir = "$buildDir"
  $docDir = "$baseDir\Doc"
  $releaseDir = "$buildDir\Release"
  $workingDir = "$buildDir\Working"
  
  $apiKey = "0ba9cd9a-62b4-4ba4-9c06-f12b76fde17b"
  $nugetSource = "https://www.myget.org/F/brightspark/api/v2/package"
  
  $ns = "Sparkling.Xelect.js.Mvc.Ext"
 
  $signKeyPath = "$sourceDir\$ns\sparkling.pfx"
  $msBuildPath = "C:\Program Files (x86)\MSBuild\14.0\Bin"
  
  $builds = @(
    @{
		Root = "$ns\";
		Name = "$ns.csproj"; 
		TestsName = "$ns.Test"; 
		Constants=""; 
		FinalDir="Net40"; 
		NuGetDir = "net40"; 
		Framework="net-4.0"; 
		Sign=$true
	}
  )
}

$framework = '4.0x86'

task default -depends Test

# Ensure a clean working directory
task Clean {
  Set-Location $baseDir
  
  if (Test-Path -path $workingDir)
  {
    Write-Output "Deleting Working Directory"
    
    del $workingDir -Recurse -Force
  }
  
  New-Item -Path $workingDir -ItemType Directory
}

# Build each solution, optionally signed
task Build -depends Clean { 
	
	if ($updateVersionNumber)
	{
		Write-Host -ForegroundColor Green "Updating assembly version"
	
		$assemblyInfoCs = "$sourceDir\$ns\Properties\AssemblyInfo.cs"
		Write-Host $assemblyInfoCs
	
		Write-Host
		&"C:\Program Files (x86)\Microsoft Visual Studio 11.0\Common7\IDE\tf.exe" checkout $assemblyInfoCs
	
		Update-AssemblyInfoFiles "$sourceDir\$ns"
	}
	else
	{
		Write-Host -ForegroundColor Yellow "Skip Updating assembly version"
	}

	foreach ($build in $builds)
	{
		$name = $build.Name
		$finalDir = $build.FinalDir
		$sign = ($build.Sign -and $signAssemblies)

		Write-Host -ForegroundColor Green "Building " $name
		Write-Host -ForegroundColor Green "Signed " $sign
		Write-Host
		exec { & "$msbuildPath\msbuild" "/t:Clean;Rebuild" /p:Configuration=Release /p:DebugSymbols=true "/p:Platform=Any CPU" /p:OutputPath=bin\Release\$finalDir\ /p:AssemblyOriginatorKeyFile=$signKeyPath "/p:SignAssembly=$sign" "/p:TreatWarningsAsErrors=$treatWarningsAsErrors" (GetConstants $build.Constants $sign) ($sourceDir + "\" + $build.Root + $build.Name) | Out-Default } "Error building $name"
	}
}

# Optional build documentation, add files to final zip
task Package -depends Build {
  
  foreach ($build in $builds)
  {
    $name = $build.TestsName
	$root = $build.Root
    $finalDir = $build.FinalDir
    	
    robocopy "$sourceDir\$root\bin\Release\$finalDir" $workingDir\Package\Bin\$finalDir $ns* /NP /XO /XF *.pri | Out-Default
  }
  
  if ($buildNuGet)
  {
    New-Item -Path $workingDir\NuGet -ItemType Directory
    Copy-Item -Path "$buildDir\$ns.nuspec" -Destination $workingDir\NuGet\$ns.nuspec -recurse
    
    foreach ($build in $builds)
    {
      if ($build.NuGetDir -ne $null)
      {
        $name = $build.TestsName
		$root = $build.Root
        $finalDir = $build.FinalDir
        $frameworkDirs = $build.NuGetDir.Split(",")
        
        foreach ($frameworkDir in $frameworkDirs)
        {
          robocopy $workingDir\Package\Bin\$finalDir $workingDir\NuGet\lib\$frameworkDir /NP /XO /XF *.pri | Out-Default
        }
      }
    }
  
	$fw = $frameworkDirs[0];
	$currentVersion = GetVersion $workingDir\NuGet\lib\$fw\$ns.dll
  
    exec { .\.nuget\NuGet.exe pack $workingDir\NuGet\$ns.nuspec -Symbols -version $currentVersion }
    move -Path .\*.nupkg -Destination $workingDir\NuGet
	
	$nupkg = "$workingDir\NuGet\$ns.$currentVersion.symbols.nupkg"	

	$uploadNuGet = Read-Host 'Upload nuget? [y/n]'
	if ($uploadNuGet -eq "y")
		{
		if ($nugetSource -eq $null)
		{
			exec { .\.nuget\NuGet.exe setApiKey $apiKey }
			exec { .\.nuget\NuGet.exe push $nupkg }
		}
		else
		{
			exec { .\.nuget\NuGet.exe setApiKey $apiKey -s $nugetSource }
			exec { .\.nuget\NuGet.exe push $nupkg -s $nugetSource }
		}
	}
  }
}

# Unzip package to a location
task Deploy -depends Package {
  echo deploy
}

# Run tests on deployed files
task Test -depends Deploy {
  echo test
}


function GetConstants($constants, $includeSigned)
{
  $signed = switch($includeSigned) { $true { ";SIGNED" } default { "" } }

  return "/p:DefineConstants=`"CODE_ANALYSIS;TRACE;$constants$signed`""
}

function GetVersion([string] $file)
{
	return [System.Diagnostics.FileVersionInfo]::GetVersionInfo($file).ProductVersion
}

function Update-AssemblyInfoFiles ([string] $sourceDir)
{
    $assemblyVersionPattern = 'AssemblyVersion\("(.+)(\.([0-9]+)){1,3}"\)'
    $fileVersionPattern = 'AssemblyFileVersion\("(.+)(\.([0-9]+)){1,3}"\)'
    
    Get-ChildItem -Path $sourceDir -r -filter AssemblyInfo.cs | ForEach-Object {
        
        $filename = $_.Directory.ToString() + '\' + $_.Name
        Write-Host $filename
        $filename + ' -> ' + $version
    
        (Get-Content $filename) | ForEach-Object {
		
			$vesion = ""
			if ($_ -match $assemblyVersionPattern)
			{
				$majorVersion = $matches[1]
				$minorVersion = [int]$matches[3] + 1;
				$version = ($majorVersion + "." + $minorVersion)
			}
			$assemblyVersion = 'AssemblyVersion("' + $version + '")';
			$fileVersion = 'AssemblyFileVersion("' + $version + '")';
		
            % {$_ -replace $assemblyVersionPattern, $assemblyVersion } |
            % {$_ -replace $fileVersionPattern, $fileVersion }
        } | Set-Content $filename -encoding UTF8
    }
}