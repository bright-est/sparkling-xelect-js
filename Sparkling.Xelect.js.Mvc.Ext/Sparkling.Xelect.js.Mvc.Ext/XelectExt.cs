﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc.Html;
using Sparkling.Xelect.js.Mvc.Ext;
using Sparkling.Xelect.js.Mvc.Ext.Helpers;

namespace System.Web.Mvc
{
    internal static class HtmlHelperExt
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public static Dictionary<string, string> GetHtmlAttributesDictionary(object o)
        {
            return ObjectExt
                .ToDictionary(o)
                .Where(x => x.Value != null)
                .ToDictionary(x => x.Key.Replace('_', '-'), x => x.Value.ToString());
        }
    }

    public static class XelectExt
    {
        public static MvcHtmlString ComboboxFor<T, TProp>(this HtmlHelper<T> helper,
            Expression<Func<T, TProp>> selectedValue, IEnumerable<SelectListItem> options,
            string defaultValue = null, object htmlAttributes = null, string template = null)
        {
            var dict = HtmlHelperExt.GetHtmlAttributesDictionary(htmlAttributes);
            dict["type"] = "combobox";

            return helper.SelectorFor(selectedValue, null, options, defaultValue, dict, template);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="helper"></param>
        /// <param name="selectedValues"></param>
        /// <param name="options"></param>
        /// <param name="defaultValue"></param>
        /// <param name="htmlAttributes"></param>
        /// <returns></returns>
        public static MvcHtmlString ComboboxFor<T>(this HtmlHelper<T> helper,
            Expression<Func<T, XelectCollection>> selectedValues, IEnumerable<SelectListItem> options,
            string defaultValue = null, object htmlAttributes = null, string template = null)
        {
            var dict = HtmlHelperExt.GetHtmlAttributesDictionary(htmlAttributes);
            dict["type"] = "combobox";

            return helper.XelectFor(selectedValues, options, defaultValue, dict, template);
        }

        public static MvcHtmlString AutocompleteFor<T>(this HtmlHelper<T> helper, Expression<Func<T,
            XelectCollection>> selectedValues, string url = null, object htmlAttributes = null, string template = null)
        {
            var dict = HtmlHelperExt.GetHtmlAttributesDictionary(htmlAttributes);
            dict["type"] = "autocomplete";
            dict["data-xelect-ac-url"] = url;

            return helper.XelectFor(selectedValues, null, null, dict, template);
        }

        public static MvcHtmlString AutocompleteFor<T>(this HtmlHelper<T> helper, Expression<Func<T, string>> value,
            string url = null, object htmlAttributes = null, string template = null)
        {
            var dict = HtmlHelperExt.GetHtmlAttributesDictionary(htmlAttributes);
            dict["type"] = "autocomplete";
            dict["data-xelect-ac-url"] = url;

            return helper.SelectorFor(value, null, null, null, dict, template);
        }

        public static MvcHtmlString AutocompleteFor<T, TProp>(this HtmlHelper<T> helper, Expression<Func<T, TProp>> value,
            string url = null, object htmlAttributes = null, string template = null)
        {
            var dict = HtmlHelperExt.GetHtmlAttributesDictionary(htmlAttributes);
            dict["type"] = "autocomplete";
            dict["data-xelect-ac-url"] = url;

            return helper.SelectorFor(value, null, null, null, dict, template);
        }

        public static MvcHtmlString AutocompleteFor<T, TProp>(this HtmlHelper<T> helper, Expression<Func<T, TProp>> value,
            Expression<Func<T, string>> text, string url = null, object htmlAttributes = null, string template = null)
        {
            var dict = HtmlHelperExt.GetHtmlAttributesDictionary(htmlAttributes);
            dict["type"] = "autocomplete";
            dict["data-xelect-ac-url"] = url;

            return helper.SelectorFor(value, text, null, null, dict, template);
        }

        public static MvcHtmlString Autocomplete<T>(this HtmlHelper<T> helper, string id, string url = null,
            object htmlAttributes = null, string selectedValue = null, string selectedText = null, string template = null)
        {
            var dict = HtmlHelperExt.GetHtmlAttributesDictionary(htmlAttributes);
            var parameters = new XelectParameters
            {
                RootAttributesDictionary = dict
            };

            return helper.Autocomplete(id, url, parameters, selectedValue, selectedText, template);
        }
        public static MvcHtmlString Autocomplete<T>(this HtmlHelper<T> helper, string id, string url = null,
            XelectParameters parameters = null, string selectedValue = null, string selectedText = null, string template = null)
        {
            if (parameters == null)
            {
                parameters = new XelectParameters
                {
                    RootAttributesDictionary = new Dictionary<string, string>()
                };
            }
            else if (parameters.RootAttributesDictionary == null)
            {
                parameters.RootAttributesDictionary = new Dictionary<string, string>();
            }

            var dict = parameters.RootAttributesDictionary;
            dict["type"] = "autocomplete";
            dict["data-xelect-ac-url"] = url;

            if (!dict.ContainsKey("name"))
            {
                dict["name"] = id;
            }

            return helper.Selector(id, null, parameters, selectedValue, selectedText, template);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="helper"></param>
        /// <param name="selectedValuesPredicate"></param>
        /// <param name="options"></param>
        /// <param name="defaultValue"></param>
        /// <param name="htmlAttributes"></param>
        /// <returns></returns>
        public static MvcHtmlString XelectFor<T>(this HtmlHelper<T> helper, Expression<Func<T, XelectCollection>> selectedValuesPredicate,
            IEnumerable<SelectListItem> options, string defaultValue = null, Dictionary<string, string> htmlAttributes = null, string template = null)
        {
            return helper.SelectorFor(selectedValuesPredicate, options, defaultValue, htmlAttributes, template);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="helper"></param>
        /// <param name="selectedValuesPredicate"></param>
        /// <param name="options"></param>
        /// <param name="title"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public static MvcHtmlString SelectorFor<T>(this HtmlHelper<T> helper, Expression<Func<T, XelectCollection>> selectedValuesPredicate,
            IEnumerable<SelectListItem> options, string title = null, Dictionary<string, string> parameters = null, string template = null)
        {
            var name = helper.NameFor(selectedValuesPredicate);
            var id = helper.IdFor(selectedValuesPredicate);
            var selectedValuesCollection = selectedValuesPredicate.Compile()(helper.ViewData.Model);
            var selectedValues = selectedValuesCollection?.ToDictionary(x => x.Key.ToString(), x => x.Value);

            var model = new XelectViewModel
            {
                Id = id,
                Name = name,
                SelectedValues = selectedValues,
                Options = options,
                Title = title,
                IsMultiSelect = true
            };

            var xelectParameters = new XelectParameters
            {
                RootAttributesDictionary = parameters
            };

            ProcessParameters(model, xelectParameters);
            model.CssClass += " multiselect";

            return RenderPartial(helper, model);
        }

        public static MvcHtmlString SelectorFor<T, TProp>(this HtmlHelper<T> helper,
            Expression<Func<T, TProp>> selectedValuePredicate,
            Expression<Func<T, string>> selectedTextPredicate,
            IEnumerable<SelectListItem> options,
            string title = null,
            Dictionary<string, string> htmlAttributes = null,
            string template = null)
        {
            var parameters = new XelectParameters
            {
                RootAttributesDictionary = htmlAttributes
            };
            return helper.SelectorFor(selectedValuePredicate, selectedTextPredicate, options, title, parameters, template);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TProp"></typeparam>
        /// <param name="helper"></param>
        /// <param name="selectedValuePredicate"></param>
        /// <param name="selectedTextPredicate"></param>
        /// <param name="options"></param>
        /// <param name="title"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public static MvcHtmlString SelectorFor<T, TProp>(this HtmlHelper<T> helper,
            Expression<Func<T, TProp>> selectedValuePredicate,
            Expression<Func<T, string>> selectedTextPredicate,
            IEnumerable<SelectListItem> options,
            string title = null,
            XelectParameters parameters = null,
            string template = null)
        {
            var id = helper.IdFor(selectedValuePredicate);
            var name = helper.NameFor(selectedValuePredicate);

            MvcHtmlString textFieldId = null;
            MvcHtmlString textFieldName = null;

            // prevent multiple enumerations on ienumerable
            options = options?.ToArray();

            var selectedValue = selectedValuePredicate.Compile()(helper.ViewData.Model)?.ToString();
            string selectedText = null;

            if (parameters == null)
            {
                parameters = new XelectParameters();
            }

            if (selectedTextPredicate != null)
            {
                textFieldId = helper.IdFor(selectedTextPredicate);
                textFieldName = helper.NameFor(selectedTextPredicate);
                selectedText = selectedTextPredicate.Compile()(helper.ViewData.Model);
            }
            else
            {
                if (options != null)
                {
                    selectedText = selectedValue == null
                        ? null
                        : options.FirstOrDefault(x => x.Value == selectedValue)?.Text;
                }
                else if (parameters.TextInputAttributesDictionary != null)
                {
                    selectedText = parameters.TextInputAttributesDictionary.GetValueOrDefault("value");
                }

                if (parameters.TextInputAttributesDictionary != null)
                {
                    textFieldId = new MvcHtmlString(parameters.TextInputAttributesDictionary.GetValueOrDefault("id"));
                    textFieldName = new MvcHtmlString(parameters.TextInputAttributesDictionary.GetValueOrDefault("name"));
                }
            }

            var model = new XelectViewModel
            {
                Id = id,
                Name = name,
                TextFieldId = textFieldId,
                TextFieldName = textFieldName,
                SelectedValues = null,
                SelectedValue = selectedValue,
                SelectedText = selectedText,
                Options = options,
                Title = title,
            };

            var modelState = helper.ViewData.ModelState[name.ToString()];
            if (modelState != null && modelState.Errors.Count > 0)
            {
                if (parameters.TextInputAttributesDictionary == null)
                {
                    parameters.TextInputAttributesDictionary = new Dictionary<string, string>();
                }
                parameters.TextInputAttributesDictionary["class"] = parameters.TextInputAttributesDictionary.GetValueOrDefault("class") + " input-validation-error";
            }

            ProcessParameters(model, parameters);

            return RenderPartial(helper, model, template);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="helper"></param>
        /// <param name="id"></param>
        /// <param name="options"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public static MvcHtmlString Selector<T>(
            this HtmlHelper<T> helper,
            string id,
            IEnumerable<SelectListItem> options,
            XelectParameters parameters = null,
            string selectedValue = null,
            string selectedText = null,
            string template = null)
        {
            var model = new XelectViewModel
            {
                Id = new MvcHtmlString(id),
                SelectedValues = null,
                SelectedText = selectedText,
                SelectedValue = selectedValue,
                Options = options,
            };

            ProcessParameters(model, parameters);

            return RenderPartial(helper, model, template);
        }

        private static void ProcessParameters(XelectViewModel model, XelectParameters parameters)
        {
            if (model == null || parameters == null)
            {
                return;
            }

            if (parameters.RootAttributesDictionary != null)
            {
                model.Type = parameters.RootAttributesDictionary.GetValueOrDefault("type", "combobox");
                parameters.RootAttributesDictionary.Remove("type");

                model.AsCombobox = model.Type.Equals("combobox", StringComparison.InvariantCultureIgnoreCase);
                model.AsAutocomplete = model.Type.Equals("autocomplete", StringComparison.InvariantCultureIgnoreCase);
                model.AsDropdown = model.Type.Equals("dropdown", StringComparison.InvariantCultureIgnoreCase);

                model.TabIndex = parameters.RootAttributesDictionary.GetValueOrDefault("tabindex");
                model.Src = parameters.RootAttributesDictionary.GetValueOrDefault("src");
                model.CssClass = parameters.RootAttributesDictionary.GetValueOrDefault("class");
                parameters.RootAttributesDictionary.Remove("class");

                if (model.AsCombobox)
                {
                    model.CssClass += " xelect-combobox";
                    parameters.RootAttributesDictionary["data-xelect-type"] = "combobox";
                }
                else if (model.AsAutocomplete)
                {
                    model.CssClass += " xelect-autocomplete";
                    parameters.RootAttributesDictionary["data-xelect-type"] = "autocomplete";
                }
                else
                {
                    model.CssClass += " xelect-select";
                    parameters.RootAttributesDictionary["data-xelect-type"] = "select";
                }

                var disabled = parameters.RootAttributesDictionary.GetValueOrDefault("disabled");

                model.Disabled = new[] { "true", "disabled" }.Contains(disabled, StringComparer.InvariantCultureIgnoreCase);

                if (model.Disabled)
                {
                    model.CssClass += " disabled";
                }
                model.CssClass = model.CssClass.Trim();

                if (parameters.RootAttributesDictionary.ContainsKey("name"))
                {
                    model.Name = new MvcHtmlString(parameters.RootAttributesDictionary["name"]);
                }

                if (parameters.RootAttributesDictionary.ContainsKey("title"))
                {
                    model.Title = parameters.RootAttributesDictionary["title"];
                }
            }

            if (model.SelectedValue == null && parameters.ValueInputAttributesDictionary != null 
                && parameters.ValueInputAttributesDictionary.ContainsKey("value"))
            {
                model.SelectedValue = parameters.ValueInputAttributesDictionary["value"];
                parameters.ValueInputAttributesDictionary.Remove("value");
            }

            if (model.SelectedText == null && parameters.TextInputAttributesDictionary != null
                && parameters.TextInputAttributesDictionary.ContainsKey("value"))
            {
                model.SelectedText = parameters.TextInputAttributesDictionary["value"];
                parameters.TextInputAttributesDictionary.Remove("value");
            }

            model.Attributes = parameters;
        }

        public static string GetXelectAttrString(this HtmlHelper helper, IDictionary<string, string> dict)
        {
            if (dict == null)
            {
                return null;
            }

            var attrs = dict.Select(x => string.Format("{0}='{1}'", x.Key, x.Value));

            return string.Join(" ", attrs);
        }

        private static MvcHtmlString RenderPartial<T>(HtmlHelper<T> helper, XelectViewModel model, string template = null)
        {
            return RenderConfig.DefaultRenderEngine.Render(model, helper.ViewContext, template ?? RenderConfig.ViewNames.Xelect);
        }
    }
}