﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Sparkling.Xelect.js.Mvc.Ext.Helpers
{
    internal static class ObjectExt
    {
        public static IDictionary<string, object> ToDictionary(object o)
        {
            var res = new Dictionary<string, object>();

            if (o != null) {
                res = o.GetType()
                    .GetProperties(BindingFlags.Public | BindingFlags.Instance)
                    .ToDictionary(x => x.Name, x => x.GetValue(o, null));
            }

            return res;
        }

        public static T GetAttrValue<TAttr, T>(this Type t, Func<TAttr, T> f) where TAttr : Attribute
        {
            var attributes = t.GetCustomAttributes(typeof(TAttr)).ToArray();
            if (attributes.Length == 0) {
                return default(T);
            }

            return f(attributes[0] as TAttr);
        }

        public static TValue GetValueOrDefault<TKey, TValue>(this Dictionary<TKey, TValue> dict, TKey key, TValue defaultValue = default(TValue))
        {
            if (!dict.ContainsKey(key)) {
                return defaultValue;
            }

            return dict[key];
        }
    }
}
