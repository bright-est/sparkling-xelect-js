﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiSelect.js.Mvc.Ext
{
    public class MultiselectViewModel
    {
        public MvcHtmlString Name { get; set; }
        public MvcHtmlString Id { get; set; }

        public Dictionary<string, string> SelectedValues { get; set; }

        public string SelectedText { get; set; }
        public string SelectedValue { get; set; }

        public IEnumerable<SelectListItem> Options { get; set; }
        public string Title { get; set; }
        public string TabIndex { get; set; }
        public bool AsCombobox { get; set; }
        public bool AsAutocomplete { get; set; }
        public bool AsDropdown { get; set; }
        public string CssClass { get; set; }
        public string Src { get; set; }
        public bool IsMultiSelect { get; set; }
        public bool Disabled { get; set; }
        public MvcHtmlString TextFieldId { get; set; }
        public MvcHtmlString TextFieldName { get; set; }
    }

    public class MultiSelectCollection : Dictionary<int, string>
    {
        public MultiSelectCollection() { }

        public MultiSelectCollection(IEnumerable<KeyValuePair<int, string>> collection)
        {
            if (collection == null)
                return;

            foreach (var kvp in collection)
            {
                this[kvp.Key] = kvp.Value;
            }
        }

        public int[] GetSelectedValues()
        {
            return Keys.ToArray();
        }
    }

    public static class MultiSelectCollectionExt
    {
        public static string Json(this MultiSelectCollection collection)
        {
            if (collection == null)
                return "[]";

            return collection.Select(x => new { id = x.Key, name = x.Value }).ToArray().ToJson();
        }
    }
}
