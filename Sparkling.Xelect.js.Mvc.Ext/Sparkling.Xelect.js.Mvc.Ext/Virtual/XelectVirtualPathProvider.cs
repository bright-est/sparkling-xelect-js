﻿using System;
using System.Collections;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Hosting;

namespace Sparkling.Xelect.js.Mvc.Ext.Virtual
{
    public class XelectVirtualPathProvider : VirtualPathProvider
    {
        private bool IsEmbeddedResourcePath(string virtualPath)
        {
            var checkPath = VirtualPathUtility.ToAppRelative(virtualPath);

            // Check for .cshtml ending because we have a Razor view and the WebForms engine complains when it gets a Razor view
            var isEmbeddedResource = Path.GetExtension(virtualPath) == ".cshtml" && RenderConfig.ViewNames.AvailableViewNames.Any(x => checkPath.Contains(x));

            return isEmbeddedResource;
        }

        public override bool FileExists(string virtualPath)
        {
            return IsEmbeddedResourcePath(virtualPath) || base.FileExists(virtualPath);
        }

        public override VirtualFile GetFile(string virtualPath)
        {
            return IsEmbeddedResourcePath(virtualPath)
                ? new XelectVirtualFile(virtualPath)
                : base.GetFile(virtualPath);
        }

        public override CacheDependency GetCacheDependency(string virtualPath, IEnumerable virtualPathDependencies, DateTime utcStart)
        {
            return IsEmbeddedResourcePath(virtualPath)
                ? null
                : base.GetCacheDependency(virtualPath, virtualPathDependencies, utcStart);
        }
    }
}
