using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Hosting;

namespace Sparkling.Xelect.js.Mvc.Ext.Virtual
{
    public class XelectVirtualFile : VirtualFile
    {
        private string path;

        //private static System.Reflection.Assembly GridAssembly;

        public XelectVirtualFile(string virtualPath) : base(virtualPath)
        {
            path = VirtualPathUtility.ToAppRelative(virtualPath);
        }

        public override Stream Open()
        {
            var parts = path.Split('/');
            var assemblyName = parts[1];
            var resourceName = path.Substring(2).Replace('/', '.');

            //var fullPathForAssembly = Path.Combine(HttpRuntime.BinDirectory, assemblyName + ".dll");
            //var bytes = File.ReadAllBytes(fullPathForAssembly);

            //var assembly = System.Reflection.Assembly.Load(bytes);

            var assembly = Assembly.GetExecutingAssembly();

            var ms = new MemoryStream();

            var correctResourceName = GetCorrectResourceName(resourceName);

            if (assembly != null) {
                using (var manifestResourceStream = assembly.GetManifestResourceStream(correctResourceName)) {
                    if (manifestResourceStream != null) {
                        manifestResourceStream.CopyTo(ms);
                        ms.Seek(0, SeekOrigin.Begin);
                    }
                }
            }

            return ms;
        }

        private string GetCorrectResourceName(string resourceName)
        {
            var fileName = resourceName.Split('.').Reverse().Skip(1).FirstOrDefault();

            return Assembly.GetExecutingAssembly().GetManifestResourceNames().FirstOrDefault(x => x.Contains(fileName));
        }
    }
}