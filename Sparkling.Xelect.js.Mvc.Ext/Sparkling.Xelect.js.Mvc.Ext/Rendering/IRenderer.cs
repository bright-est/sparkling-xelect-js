﻿using System.Web.Mvc;

namespace Sparkling.Xelect.js.Mvc.Ext.Rendering
{
    public interface IRenderer
    {
        MvcHtmlString Render<T>(T model, ViewContext context, string viewName);
    }
}