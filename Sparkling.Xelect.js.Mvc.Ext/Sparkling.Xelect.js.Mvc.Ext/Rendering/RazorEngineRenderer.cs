﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sparkling.Xelect.js.Mvc.Ext.Rendering
{
    public class RazorEngineRenderer : IRenderer
    {
        public MvcHtmlString Render<T>(T model, ViewContext context, string viewName)
        {
            var viewResult = ViewEngines.Engines.FindPartialView(context, viewName);
            var view = viewResult.View;

            if (view == null) {
                return MvcHtmlString.Create(string.Format("view '{0}' was not found", viewName));
            }

            var viewDataDictionary = new ViewDataDictionary(model);

            foreach (var kvp in context.Controller.ViewData) {
                viewDataDictionary[kvp.Key] = kvp.Value;
            }

            using (var sw = new StringWriter()) {
                ViewContext viewContext = new ViewContext(context, view, viewDataDictionary, context.Controller.TempData, sw);
                view.Render(viewContext, sw);

                return MvcHtmlString.Create(sw.GetStringBuilder().ToString());
            }  
        }
    }
}