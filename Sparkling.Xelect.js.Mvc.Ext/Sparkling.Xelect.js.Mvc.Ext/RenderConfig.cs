﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using Sparkling.Xelect.js.Mvc.Ext.Rendering;
using Sparkling.Xelect.js.Mvc.Ext.Virtual;

namespace Sparkling.Xelect.js.Mvc.Ext
{
    public static class RenderConfig
    {
        public static IRenderer DefaultRenderEngine { get; set; } = new RazorEngineRenderer();

        public static class ViewNames
        {
            private const string XELECT = "_Xelect_Default_View";
            public static string Xelect { get; set; } = XELECT;

            public static IEnumerable<string> AvailableViewNames { get { yield return XELECT; } }
        }

        static RenderConfig()
        {
            HostingEnvironment.RegisterVirtualPathProvider(new XelectVirtualPathProvider());
        }
    }
}