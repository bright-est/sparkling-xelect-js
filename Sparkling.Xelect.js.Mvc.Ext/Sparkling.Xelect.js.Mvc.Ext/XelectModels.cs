﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Sparkling.Xelect.js.Mvc.Ext.Helpers;

namespace Sparkling.Xelect.js.Mvc.Ext
{
    public class XelectViewModel
    {
        public MvcHtmlString Name { get; set; }
        public MvcHtmlString Id { get; set; }

        public Dictionary<string, string> SelectedValues { get; set; }

        public string SelectedText { get; set; }
        public string SelectedValue { get; set; }

        public IEnumerable<SelectListItem> Options { get; set; }
        public string Title { get; set; }
        public string TabIndex { get; set; }
        public string Type { get; set; }
        public bool AsCombobox { get; set; }
        public bool AsAutocomplete { get; set; }
        public bool AsDropdown { get; set; }
        public string CssClass { get; set; }
        public string Src { get; set; }
        public bool IsMultiSelect { get; set; }
        public bool Disabled { get; set; }
        public MvcHtmlString TextFieldId { get; set; }
        public MvcHtmlString TextFieldName { get; set; }

        public XelectParameters Attributes { get; set; }
    }

    public class XelectCollection : Dictionary<int, string>
    {
        public XelectCollection() { }

        public XelectCollection(IEnumerable<KeyValuePair<int, string>> collection)
        {
            if (collection == null) {
                return;
            }

            foreach (var kvp in collection) {
                this[kvp.Key] = kvp.Value;
            }
        }

        public int[] GetSelectedValues()
        {
            return Keys.ToArray();
        }
    }

    public class XelectParameters
    {
        private Dictionary<string, string> _rootAttrs;
        private Dictionary<string, string> _valueInputAttrs;
        private Dictionary<string, string> _textInputAttrs;

        public object RootAttributes
        {
            set { _rootAttrs = HtmlHelperExt.GetHtmlAttributesDictionary(value); }
        }
        public object ValueInputAttributes
        {
            set { _valueInputAttrs = HtmlHelperExt.GetHtmlAttributesDictionary(value); }
        }
        public object TextInputAttributes
        {
            set { _textInputAttrs = HtmlHelperExt.GetHtmlAttributesDictionary(value); }
        }

        public Dictionary<string, string> RootAttributesDictionary
        {
            get
            {
                return _rootAttrs;
            }
            set { _rootAttrs = value; }
        }

        public Dictionary<string, string> ValueInputAttributesDictionary
        {
            get
            {
                return _valueInputAttrs;
            }
            set { _valueInputAttrs = value; }
        }

        public Dictionary<string, string> TextInputAttributesDictionary
        {
            get
            {
                return _textInputAttrs;
            }
            set { _textInputAttrs= value; }
        }
    }

    public static class XelectCollectionExt
    {
        public static string Json(this XelectCollection collection)
        {
            if (collection == null) {
                return "[]";
            }

            return collection.Select(x => new { id = x.Key, name = x.Value }).ToArray().ToJson();
        }
    }
}
