﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Sparkling.Xelect.js.Mvc.Demo.Models;

namespace Sparkling.Xelect.js.Mvc.Demo.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var model = new DemoModel()
            {
                
            };

            return View(model);
        }
    }
}