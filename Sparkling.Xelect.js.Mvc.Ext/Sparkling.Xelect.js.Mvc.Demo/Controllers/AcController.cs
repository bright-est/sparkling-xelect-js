﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Sparkling.Xelect.js.Mvc.Demo.Models;

namespace Sparkling.Xelect.js.Mvc.Demo.Controllers
{
    public class AcController : Controller
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="typeId"></param>
        /// <param name="q"></param>
        /// <param name="excludeIds"></param>
        /// <returns></returns>
        public ActionResult Clv(int? typeId, string q, int[] excludeIds)
        {
            var values = new List<Clv>()
            {
                new Clv() { Id = 1, Value = "Test1", TypeId = 1 },
                new Clv() { Id = 2, Value = "Test20", TypeId = 1 },
                new Clv() { Id = 3, Value = "Test300", TypeId = 3 },
                new Clv() { Id = 4, Value = "Test4000", TypeId = 4 },
                new Clv() { Id = 5, Value = "Test50000", TypeId = 7 },
                new Clv() { Id = 6, Value = "Test600000", TypeId = 7 },
                new Clv() { Id = 7, Value = "Test610000", TypeId = 7 },
                new Clv() { Id = 8, Value = "Test620000", TypeId = 7 },
                new Clv() { Id = 9, Value = "Test630000", TypeId = 7 },
                new Clv() { Id = 10, Value = "Test640000", TypeId = 7 },
            };

            var seed = values
                .Where(x => x.Value.ToLower().Contains(q.ToLower())
                    && (!typeId.HasValue || x.TypeId == typeId));

            //prevent from reordering as the correct order is already set
            return Res(q, seed, x => x.Value, excludeIds, orderByName: false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="q"></param>
        /// <param name="excludeIds"></param>
        /// <returns></returns>
        public ActionResult Contractor(string q, int[] excludeIds)
        {
            var values = new List<Contact>()
            {
                new Contact() { Id = 1, Name = "Name1" },
                new Contact() { Id = 2, Name = "Name20" },
                new Contact() { Id = 3, Name = "Name300" },
                new Contact() { Id = 4, Name = "Name4000" },
                new Contact() { Id = 5, Name = "Name50000" },
                new Contact() { Id = 6, Name = "Name600000" }
            };

            var seed = values
                .Where(x => x.Name.ToLower().Contains(q.ToLower()));

            return Res(q, seed, x => x.Name, excludeIds);
        }

        private ActionResult Res<T>(string q, IEnumerable<T> seed, Func<T, string> nameSelector, int[] excludeIds = null, int? limit = null, bool orderByName = true) where T : Entity
        {
            if (excludeIds == null)
            {
                excludeIds = new int[0];
            }

            var query = seed
                .Where(x => !excludeIds.Contains(x.Id))
                .Select(
                    x =>
                        new
                        {
                            i = nameSelector(x).IndexOf(q, StringComparison.OrdinalIgnoreCase),
                            value = new { id = x.Id, name = nameSelector(x) }
                        });

            if (orderByName)
            {
                query = query.OrderBy(x => x.i);
            }

            var res = query
                .Take(limit ?? 25)
                .Select(x => x.value)
                .ToArray();

            return Json(res, JsonRequestBehavior.AllowGet);
        }
    }
}