﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sparkling.Xelect.js.Mvc.Ext;

namespace Sparkling.Xelect.js.Mvc.Demo.Models
{
    public class DemoModel
    {
        public int? ContractorId { get; set; }
        public string ContractorName { get; set; }

        public int? BuyInGroupId { get; set; }
        public string BuyInGroupName { get; set; }

        public XelectCollection Names { get; set; }
    }
}