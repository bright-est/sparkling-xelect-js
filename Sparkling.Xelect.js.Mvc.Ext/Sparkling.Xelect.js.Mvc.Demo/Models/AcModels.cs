﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sparkling.Xelect.js.Mvc.Demo.Models
{
    public class Entity
    {
        public int Id { get; set; }
    }

    public class Contact : Entity
    {
        public string Name { get; set; }
    }

    public class Clv : Entity
    {
        public string Value { get; set; }
        public int TypeId { get; set; }
    }
}